import os
import tkinter as tk
from tkinter import ttk, Frame
from datetime import date
import Writing


#***********************************************************************************
class FolderExplorer(tk.Frame):
    def __init__(self, DCForm1_, master=None):
        super().__init__(master)
        self.master = master
        self.DCForm1_ = DCForm1_
        self.current_folder = os.getcwd()
        self.folder_listbox = None

        # Text box
        self.text1 = tk.Text(self.master, height=10, width=30)
        self.text1.pack(side=tk.RIGHT, padx=10)
        self.text1.bind("<Button-1>", self.open_file)

        self.create_widgets()
        self.create_root_folder()
        self.list_files()

    def list_files(self):
        os.chdir(self.current_folder)
        self.text1.delete("1.0","end")
        # Add the names of files in the current directory to the text box
        for filename in os.listdir("."):
            #if filename.endswith(".py"):
            self.text1.insert(tk.END, filename + "\n")

    def open_file(self, event):
        index = self.text1.index(tk.CURRENT)
        filename = self.text1.get(index + "linestart", index + "lineend")
        try:
            if (".pp" in filename) or (".cc" in filename) or (".VC" in filename) or (".us" in filename) \
                    or (".bg" in filename) or (".smc" in filename) or (".mn" in filename):
                Writing.DCReadPastTrace(filename, self.DCForm1_)
            else:
                with open(filename) as file:
                    contents = file.read()
                    tk.messagebox.showinfo("File Contents", contents)
        except IOError:
            tk.messagebox.showerror("Error", "Could not open file: " + filename)

    def create_widgets(self):
        # Create the folder listbox
        self.folder_listbox = tk.Listbox(self.master)
        self.folder_listbox.pack(side="left", fill="both", expand=True)
        self.update_folder_listbox()
        # Bind double-click on folder listbox to change current folder
        self.folder_listbox.bind("<Double-Button-1>", self.change_folder)
        # Create the new folder Button
        new_folder_Button = tk.Button(self.master, text="Create Folder", command=self.create_new_folder)
        new_folder_Button.pack(side="bottom")

    def update_folder_listbox(self):
        # Clear the folder listbox
        self.folder_listbox.delete(0, tk.END)
        # Add the subfolders of the current folder to the listbox
        self.folder_listbox.insert(tk.END, "..")
        for folder_name in os.listdir(self.current_folder):
            if os.path.isdir(os.path.join(self.current_folder, folder_name)):
                self.folder_listbox.insert(tk.END, folder_name)
        self.list_files()

    def change_folder(self, event):
        # Get the selected folder from the listbox
        selection = self.folder_listbox.curselection()
        if len(selection) == 1:
            selected_folder = self.folder_listbox.get(selection[0])
            # Change the current folder based on the selected option
            if selected_folder == "..":
                self.go_up_folder()
            else:
                new_folder_path = os.path.join(self.current_folder, selected_folder)
                # Change the current folder to the selected folder
                self.current_folder = new_folder_path
                self.update_folder_listbox()
        self.list_files()

    def go_up_folder(self):
        # Get the parent folder of the current folder
        parent_folder = os.path.dirname(self.current_folder)
        # Change the current folder to the parent folder
        self.current_folder = parent_folder
        self.update_folder_listbox()

    def create_new_folder(self):
        # Try to create a new folder with name "Cell_2", "Cell_3", etc.
        for i in range(1, 11):
            new_folder_name = "Cell_" + str(i)
            new_folder_path = os.path.join(self.current_folder, new_folder_name)
            if not os.path.exists(new_folder_path):
                os.mkdir(new_folder_path)
                self.update_folder_listbox()
                break

    def enter_folder(self,folder_name):
        new_folder_path = os.path.join(self.current_folder, folder_name)
        self.current_folder = new_folder_path
        self.update_folder_listbox()

    def create_root_folder(self):
        # Get the current date
        today = date.today()
        # Format the date as "YYYY_MM_DD"
        folder_name = today.strftime("%Y_%m_%d") +"\Cell_1"
        # Create the folder
        try:
            import os
            os.makedirs(folder_name)
            self.enter_folder(folder_name)
            print("Folder created:", folder_name)
        except FileExistsError:
            self.enter_folder(folder_name)
            print("Folder already exists:", folder_name)
            #self.create_new_folder()
        #self.text1.delete("1.0", "end")
        #self.list_files()
#***********************************************************************************
