# program DC_Project;
# {
#   It is the main file of the program. It lists all files of the code, except
#   those from libraries (DDSpinEdit.pas, NIDAQmx.pas and NIDAQmxCAPI_TLB.pas)
# }

from tkinter import Tk
from DCUnit1 import TDCForm1
from DCUnit9 import TDCForm9
from Acquisition import *
from ArtMode import *
from CurrentClamp import *
from Measurement import *
from NeuronO import *
from NoiseO import *
from OnlineComp import *
from ReadWrite_pars import *
from RealTime import *
from Writing import *

DCForm1 = TDCForm1()
#DCForm9 = TDCForm9()


if __name__ == "__main__":

    DCForm1.root.mainloop()
#    DCForm1.run()
