# {
#  In the case of simulation instead of acquiring real signals, the single neuron
#  model is simulated. Three signals, V, Icom and Ireal, are sent to and received
#  from the model.
# }
import numpy as np
import matplotlib.pyplot as plt
from NeuronNaK import TNeuron_NaK
from NeuronO import TNeuron_HH
import globs


def Init_ArtificialNeuron(TypeOfNeuron):
    global ANrn

    # Choose type of neuron
    if TypeOfNeuron == 1:
        # Model from Izhikevich, "INa/IK model"
        ANrn = TNeuron_NaK()
    else:
        # Model from Chow, "HH model"
        ANrn = TNeuron_HH()

    # Parameters and Initial Conditions
    ANrn.SetParametersAndInitialConditions()
    # Update values
    globs.V = ANrn.NV.V
    globs.Ireal = ANrn.NV.PSC

def Acquire_ArtificialNeuron():
    # Model Neuron
    Iind = globs.Icom # pA
    dt = globs.AcqTime # ms
    Vus_ = globs.Vus
    # One step of integration
    ANrn.MembranePotential(Iind, 0, Vus_, dt)
    # Update values
    globs.V = ANrn.NV.V
    globs.Ireal = ANrn.NV.PSC


if __name__ == "__main__":
    Init_ArtificialNeuron(TypeOfNeuron=2)   # set type of neuron model, 1-"INa/IK", 2-"HH"
    globs.Vus = -60 # mV
    globs.AcqTime =0.1 # ms
    nt_end = 1000
    tt = np.zeros(nt_end)
    UU = np.zeros(nt_end)
    II = np.zeros(nt_end)

    for i in range(0,nt_end):
        tt[i] = i * globs.AcqTime
        globs.Icom = 0
        if i>100:
            globs.Icom = 50
        #*************************
        Acquire_ArtificialNeuron()
        #*************************
        UU[i]=ANrn.NV.V
        II[i]=globs.Icom

    # Plotting
    fig, axes = plt.subplots(nrows=2)
    axes[0].plot(tt, UU, "-b", label="U", linewidth=1)
    axes[1].plot(tt, II, "-b", label="Icom", linewidth=1)
    axes[0].legend()
    axes[1].legend()
    plt.xlabel("t")
    plt.show()