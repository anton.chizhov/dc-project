# unit ReadWrite_pars;
# {
#   Previously recorded data, written in file FName, ascii-coded or binary,
#   can be read with ReadDataFile.
#
#   Parameters of the recordings are written in a separate file for each trace,
#   named FName_Params.
#   They can be read and written with
#   WriteParametersTo and ReadParametersOf.
# }

import os
import globs
import struct


def ReadDataFile(fname):

    i = 0
    ii = 0
    if_b = ("_b" in fname)

    if if_b == False:       # ASCII
        with open(fname, 'r') as bbb:
            while True:
                line = bbb.readline()
                if not line:
                    break

                parts = line.split()
                time_ = float(parts[0])
                V = float(parts[1])
                Icom = float(parts[2])
                Ireal = float(parts[3])

                globs.Arr_V[i] = V
                globs.Arr_Icom[i] = Icom
                globs.Arr_Ireal[i] = Ireal
                globs.Arr_t[i] = time_

                ii += 1
                if time_ > globs.SkipInitialInterval_in_ms:
                    i += 1

    else:                   # BINARY
        with open(fname, 'rb') as ccc:

            globs.AcqTime = struct.unpack('d', ccc.read(8))[0]
            t1 = ccc.read(256).decode('utf-8')
            sc_0 = struct.unpack('d', ccc.read(8))[0]
            sc_1 = struct.unpack('d', ccc.read(8))[0]
            sc_2 = struct.unpack('d', ccc.read(8))[0]
            off_0 = struct.unpack('d', ccc.read(8))[0]
            off_1 = struct.unpack('d', ccc.read(8))[0]
            off_2 = struct.unpack('d', ccc.read(8))[0]

            while not(ccc.closed or ccc.peek(1) == b''):
                V16     = struct.unpack('d', ccc.read(8))[0]
                Icom16  = struct.unpack('d', ccc.read(8))[0]
                Ireal16 = struct.unpack('d', ccc.read(8))[0]

                globs.Arr_V[i] = V16 * sc_0 + off_0
                globs.Arr_Icom[i] = Icom16 * sc_1 + off_1
                globs.Arr_Ireal[i] = Ireal16 * sc_2 + off_2

                time_ = ii * globs.AcqTime * globs.n_RememberInArr
                globs.Arr_t[i] = time_

                ii += 1
                if time_ > globs.SkipInitialInterval_in_ms:
                    i += 1

    globs.N_Arr = i - 1
    globs.T_end = time_

    if if_b == False:
        bbb.close()
    else:
        ccc.close()


def write_parameters_to(fname_params):
    with open(fname_params, 'w') as ccc:
        ccc.write('T_end=\n')
        ccc.write('            {:10.3f}\n'.format(globs.T_end))
        ccc.write('i_end=\n')
        ccc.write('            {:13}\n'.format(globs.i_end))
        ccc.write('AcqTime=\n')
        ccc.write('            {:10.3f}\n'.format(globs.AcqTime))
        ccc.write('t_StartStim=\n')
        ccc.write('            {:10.3f}\n'.format(globs.t_StartStim))
        ccc.write('t_EndStim=\n')
        ccc.write('            {:10.3f}\n'.format(globs.t_EndStim))
        ccc.write('V_scale=\n')
        ccc.write('            {:10.3f}\n'.format(globs.V_scale))
        ccc.write('Icom_scale=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Icom_scale))
        ccc.write('Ireal_scale=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Ireal_scale))
        ccc.write('V_offset=\n')
        ccc.write('            {:10.3f}\n'.format(globs.V_offset))
        ccc.write('Icom_offset=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Icom_offset))
        ccc.write('Ireal_offset=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Ireal_offset))
        ccc.write('dt_draw=\n')
        ccc.write('            {:10.3f}\n'.format(globs.dt_draw))
        ccc.write('Epoch=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Epoch))
        ccc.write('PP.I_min=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.I_min))
        ccc.write('PP.I_max=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.I_max))
        ccc.write('PP.N_steps=\n')
        ccc.write('            {:3}\n'.format(globs.PP.N_steps))
        ccc.write('PP.t1_start=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.t1_start))
        ccc.write('PP.t1_end=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.t1_end))
        ccc.write('PP.t2_start=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.t2_start))
        ccc.write('PP.t2_end=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.t2_end))
        ccc.write('PP.G_L=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.G_L))
        ccc.write('PP.tau_m=\n')
        ccc.write('            {:13.3f}\n'.format(globs.PP.tau_m))
        ccc.write('CC.I_stim=\n')
        ccc.write('            {:13.3f}\n'.format(globs.CC.I_stim))
        ccc.write('CC.g_stim=\n')
        ccc.write('            {:10.3f}\n'.format(globs.CC.g_stim))
        ccc.write('CC.T_I=\n')
        ccc.write('            {:10.3f}\n'.format(globs.CC.T_I))
        ccc.write('Protocol=\n')
        ccc.write('            {:3}\n'.format(globs.Protocol))
        ccc.write('Vus=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Vus))
        ccc.write('CC.T_g=\n')
        ccc.write('            {:10.3f}\n'.format(globs.CC.T_g))
        ccc.write('CC.TypeOfInputIndex=\n')
        ccc.write('            {:3}\n'.format(globs.CC.TypeOfInputIndex))
        ccc.write('CC.C_stim=\n')
        ccc.write('            {:10.3f}\n'.format(globs.CC.C_stim))
        ccc.write('EC.G1=\n')
        ccc.write('            {:10.3f}\n'.format(globs.EC.G1))
        ccc.write('DeviceNumber=\n')
        ccc.write('            {:3}\n'.format(globs.DeviceNumber))
        ccc.write('If_CondCompensation=\n')
        ccc.write('            {:3}\n'.format(globs.If_CondCompensation))
        ccc.write('EC.tau_Vfilt=\n')
        ccc.write('            {:10.3f}\n'.format(globs.EC.tau_Vfilt))
        ccc.write('EC.tau_Ifilt=\n')
        ccc.write('            {:10.3f}\n'.format(globs.EC.tau_Ifilt))
        ccc.write('Vmin=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Vmin))
        ccc.write('Vmax=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Vmax))
        ccc.write('dVmax=\n')
        ccc.write('            {:10.3f}\n'.format(globs.dVmax))
        ccc.write('Noise.tau_I=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Noise.tau_I))
        ccc.write('Noise.tau_G=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Noise.tau_G))
        ccc.write('Noise.sgm_I=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Noise.sgm_I))
        ccc.write('Noise.sgm_G=\n')
        ccc.write('            {:10.3f}\n'.format(globs.Noise.sgm_G))
        ccc.write('Subjective note:\n')
        ccc.write(' {}\n'.format(globs.SubjectiveNote))
        ccc.write('CC.IfStimWholeTime=\n')
        ccc.write('            {:3}\n'.format(globs.CC.IfStimWholeTime))
        ccc.write('dt_AcqForms=\n')
        ccc.write('            {:10.3f}\n'.format(globs.dt_AcqForms))
        ccc.write('n_RememberInArr=\n')
        ccc.write('            {:13}\n'.format(globs.n_RememberInArr))
        ccc.write('N_Arr=\n')
        ccc.write('            {:13}\n'.format(globs.N_Arr))
        ccc.write('BasicColor=\n')
        ccc.write(' {}\n'.format(globs.BasicColor))
        ccc.write('MN.I0=\n')
        ccc.write('            {:10.3f}\n'.format(globs.MN.I0))
        ccc.write('MN.tau=\n')
        ccc.write('            {:10.3f}\n'.format(globs.MN.tau))
        ccc.write('MN.alpha=\n')
        ccc.write('            {:10.3f}\n'.format(globs.MN.alpha))
        ccc.write('TypeOfModelledNeuron:\n')
        ccc.write('            {:13}\n'.format(globs.TypeOfModelledNeuron))

    ccc.close()

def ReadParametersOf(fname_params):
    if not os.path.exists(fname_params):
        return

    with open(fname_params, 'r') as ccc:
        ccc.readline()  # T_end=
        globs.T_end = float(ccc.readline().strip())
        ccc.readline()  # i_end=
        globs.i_end = int(ccc.readline().strip())
        ccc.readline()  # AcqTime=
        globs.AcqTime = float(ccc.readline().strip())
        ccc.readline()  # t_StartStim=
        globs.t_StartStim = float(ccc.readline().strip())
        ccc.readline()  # t_EndStim=
        globs.t_EndStim = float(ccc.readline().strip())
        ccc.readline()  # V_scale=
        globs.V_scale = float(ccc.readline().strip())
        ccc.readline()  # Icom_scale=
        globs.Icom_scale = float(ccc.readline().strip())
        ccc.readline()  # Ireal_scale=
        globs.Ireal_scale = float(ccc.readline().strip())
        ccc.readline()  # V_offset=
        globs.V_offset = float(ccc.readline().strip())
        ccc.readline()  # Icom_offset=
        globs.Icom_offset = float(ccc.readline().strip())
        ccc.readline()  # Ireal_offset=
        globs.Ireal_offset = float(ccc.readline().strip())
        ccc.readline()  # dt_draw=
        globs.dt_draw = float(ccc.readline().strip())
        ccc.readline()  # Epoch=
        globs.Epoch = float(ccc.readline().strip())
        ccc.readline()  # PP.I_min=
        globs.PP.I_min = float(ccc.readline().strip())
        ccc.readline()  # PP.I_max=
        globs.PP.I_max = float(ccc.readline().strip())
        ccc.readline()  # PP.N_steps=
        globs.PP.N_steps = int(ccc.readline().strip())
        ccc.readline()  # PP.t1_start=
        globs.PP.t1_start = float(ccc.readline().strip())
        ccc.readline()  # PP.t1_end=
        globs.PP.t1_end = float(ccc.readline().strip())
        ccc.readline()  # PP.t2_start=
        globs.PP.t2_start = float(ccc.readline().strip())
        ccc.readline()  # PP.t2_end=
        globs.PP.t2_end = float(ccc.readline().strip())
        ccc.readline()  # PP.G_L=
        globs.PP.G_L = float(ccc.readline().strip())
        ccc.readline()  # PP.tau_m=
        globs.PP.tau_m = float(ccc.readline().strip())
        ccc.readline()  # CC.I_stim=
        globs.CC.I_stim = float(ccc.readline().strip())
        ccc.readline()  # CC.g_stim=
        globs.CC.g_stim = float(ccc.readline().strip())
        ccc.readline()  # CC.T_I=
        globs.CC.T_I = float(ccc.readline().strip())
        ccc.readline()  # Protocol=
        globs.Protocol = int(ccc.readline().strip())
        ccc.readline()  # Vus=
        globs.Vus = float(ccc.readline().strip())
        ccc.readline()  # CC.T_g=
        globs.CC.T_g = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # CC.TypeOfInputIndex=
                globs.CC.TypeOfInputIndex = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # CC.C_stim=
                globs.CC.C_stim = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # EC.G1=
                globs.EC.G1 = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # DeviceNumber=
                globs.DeviceNumber = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # If_CondCompensation=
                globs.If_CondCompensation = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # EC.tau_Vfilt=
                globs.EC.tau_Vfilt = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # EC.tau_Ifilt=
                globs.EC.tau_Ifilt = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Vmin=
                globs.Vmin = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Vmax=
                globs.Vmax = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # dVmax=
                globs.dVmax = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Noise.tau_I=
                globs.Noise.tau_I = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Noise.tau_G=
                globs.Noise.tau_G = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Noise.sgm_I=
                globs.Noise.sgm_I = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Noise.sgm_G=
                globs.Noise.sgm_G = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # Subjective note:
                globs.SubjectiveNote = ccc.readline().strip()
        if not ccc.closed:
            if not (ccc.readline() == ''):  # CC.IfStimWholeTime=
                globs.CC.IfStimWholeTime = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # dt_AcqForms=
                globs.dt_AcqForms = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # n_RememberInArr=
                globs.n_RememberInArr = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # N_Arr=
                globs.N_Arr = int(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # BasicColor=
                globs.BasicColor = ccc.readline().strip()
        if not ccc.closed:
            if not (ccc.readline() == ''):   # MN.I0=
                globs.MN.I0 = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # MN.tau=
                globs.MN.tau = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # MN.alpha=
                globs.MN.alpha = float(ccc.readline().strip())
        if not ccc.closed:
            if not (ccc.readline() == ''):  # TypeOfModelledNeuron=
                globs.TypeOfModelledNeuron = int(ccc.readline().strip())

    ccc.close()

