import numpy as np
import CurrentClamp
import globs
import NoiseO
import OnlineComp
import Writing
import copy
from Acquisition import *
from RealTime import *
from datetime import datetime



def ZeroArrays():
#    global Arr_V, Arr_Ireal, Arr_Icom, Arr_t
    globs.Arr_V = np.zeros(globs.ArrMax)
    globs.Arr_Ireal = np.zeros(globs.ArrMax)
    globs.Arr_Icom = np.zeros(globs.ArrMax)
    globs.Arr_t = np.zeros(globs.ArrMax)


def ReadProtocolParameters(DCForm1):
    globs.Protocol = DCForm1.RadioGroup2_value.get()
    match globs.Protocol:
        case 1:  # Current-Clamp
            CurrentClamp.ReadCurrenClampParameters()
        case 2:  # Passive Properties
            Passive.ReadPassiveParameters()
        case 3:  # Tongue
            pass
        case 5:  # Dynamic VC
            pass
        case 6:  # MathNeuro
            MathNeuro.ReadMathNeuroParameters()
    # other cases are left blank


def ReadSubProtocolParameters(DCForm1):
    globs.If_CondCompensation = globs.IfTrue(DCForm1.CheckBox5_Checked.get())
    globs.If_Noise = globs.IfTrue(DCForm1.CheckBox14_Checked.get())
    if globs.If_CondCompensation:
        OnlineComp.ReadElectrodeCompensationParameters()
    if globs.If_Noise:
        NoiseO.ReadNoiseParameters(DCForm1)


def SetParametersFromForms(DCForm1):
    globs.IfNoPlotsInBackground = False
    globs.PP.I_min = float(DCForm1.DDSpinEdit5.get())
    globs.PP.I_max = float(DCForm1.DDSpinEdit6.get())
    globs.PP.N_steps = int(DCForm1.DDSpinEdit7.get())
    globs.PP.t1_start = float(DCForm1.DDSpinEdit14.get())
    globs.PP.t1_end = float(DCForm1.DDSpinEdit15.get())
    globs.PP.t2_start = float(DCForm1.DDSpinEdit16.get())
    globs.PP.t2_end = float(DCForm1.DDSpinEdit17.get())
    globs.PP.G_L = float(DCForm1.DDSpinEdit18.get())  # nS
    globs.PP.tau_m = float(DCForm1.DDSpinEdit54.get()) # ms
    globs.CC.TypeOfInputIndex = int(DCForm1.ComboBox1.current())
    globs.CC.I_stim = float(DCForm1.DDSpinEdit1.get())  # pA
    globs.CC.g_stim = float(DCForm1.DDSpinEdit37.get())  # nS
    globs.CC.C_stim = float(DCForm1.DDSpinEdit53.get())  # pF
    globs.CC.T_I = float(DCForm1.DDSpinEdit2.get())  # ms
    globs.CC.T_g = float(DCForm1.DDSpinEdit52.get())  # ms
    globs.CC.IfStimWholeTime = globs.IfTrue(DCForm1.CheckBox18_Checked.get())
    globs.Vus = float(DCForm1.DDSpinEdit30.get())  # mV
    globs.T_end = float(DCForm1.DDSpinEdit3.get())  # ms
    globs.AcqTime = float(DCForm1.DDSpinEdit4.get())  # ms
    globs.t_StartStim = float(DCForm1.DDSpinEdit9.get())  # ms
    globs.t_EndStim = float(DCForm1.DDSpinEdit10.get())  # ms
    globs.V_scale = float(DCForm1.DDSpinEdit25.get())  # mV/V
    globs.Icom_scale = float(DCForm1.DDSpinEdit26.get())  # pA/V
    globs.Ireal_scale = float(DCForm1.DDSpinEdit31.get())  # pA/V
    globs.V_offset = float(DCForm1.DDSpinEdit39.get())  # mV
    globs.Icom_offset = float(DCForm1.DDSpinEdit38.get())  # pA
    globs.Ireal_offset = float(DCForm1.DDSpinEdit40.get())  # pA
    globs.Vmin = float(DCForm1.DDSpinEdit80.get())  # mV
    globs.Vmax = float(DCForm1.DDSpinEdit81.get())  # mV
    globs.dVmax = float(DCForm1.DDSpinEdit82.get())  # mV
    globs.dt_draw = float(DCForm1.DDSpinEdit27.get())  # ms
    globs.dt_AcqForms = float(DCForm1.DDSpinEdit112.get())  # ms
    if globs.dt_draw==0:
        globs.dt_draw=globs.AcqTime
    globs.Epoch = float(DCForm1.DDSpinEdit28.get()) * 1e3  # ms
    globs.DeviceNumber = int(DCForm1.ComboBox2.current())
    globs.n_RememberInArr = int(float(DCForm1.DDSpinEdit142.get()))
    globs.MN.I0 = float(DCForm1.DDSpinEdit165.get())  # pA
    globs.MN.tau = float(DCForm1.DDSpinEdit166.get())  # ms
    globs.MN.alpha = float(DCForm1.DDSpinEdit167.get())  # nS
    globs.TypeOfModelledNeuron = int(DCForm1.ComboBox44.current())

    # ************************
    ReadProtocolParameters(DCForm1)
    # ************************
    ReadSubProtocolParameters(DCForm1)
    # ************************
    globs.i_end = int(globs.T_end / globs.AcqTime)
    globs.IfOnline = False


def Measure_Init(DCForm1):
    global ATask, CTask, DTask
    # ************************
    SetParametersFromForms(DCForm1)
    # ************************
    DAQ_Initialize_AI0_AI1_AI2_AO0(1000 / globs.AcqTime, globs.DeviceNumber, globs.TypeOfModelledNeuron)
    DAQ_Read_AI0_AI1_AI2()
    # DCForm1.InitializeDrawing()
    # Time
    current_time = datetime.now().time()
    Min = current_time.minute
    Sec = current_time.second
    MSec = current_time.microsecond // 1000
    globs.StartTime = Min * 60000 + Sec * 1000 + MSec


def Measure(DCForm1):
    # Measure
    Measure_Init(DCForm1)
    i = -1
    globs.i_Arr = -1
    while i <= globs.i_end:
        i += 1
        # ********************************
        RealTimeCalculations_at_OneStep(i)
        # ********************************

    globs.N_Arr = globs.i_Arr
    # Stop Acquisition
    DAQ_Stop_AI0_AI1_AI2_AO0_AO1_D0_D1()
    # Print Time
    now = datetime.now()
    hour, min, sec, msec = now.hour, now.minute, now.second, int(now.microsecond / 1000)
    globs.EndTime = min * 60000 + sec * 1000 + msec
    DCForm1.Label5.config(text = 'Control time=' + str(globs.EndTime - globs.StartTime) + ' ms')
    DCForm1.Label27.config(text = 'Trace number ' + str(globs.i_step))


def RunOnce(DCForm1):
    DCForm1.Button1.Enabled = False
    DCForm1.Panel1.Color = 0x002626FB
    DCForm1.Button1.configure(state="disabled")
    DCForm1.DDSpinEdit1.set(float(globs.CC.I_stim))
    DCForm1.root.update()
    globs.i_step = 1

    # **************
    Measure(DCForm1)
    # **************

    # Drawing and writing
    if globs.IfNoPlotsInBackground == False:
        DCForm1.notebook.select(DCForm1.tab1)
        DCForm1.DrawArrays()
        Writing.WriteTraceInFile(DCForm1)
    DCForm1.Button1.Enabled = True
    DCForm1.Panel1.Color = 0x8CBF26
    DCForm1.Button1.configure(state="normal") #Enabled


def RunSeries(DCForm1,IfForward):
    # Run Series
    SetParametersFromForms(DCForm1)
    #DCForm1.Button1.configure(state="disabled")

    # if Protocol == 3:  # Tongue
    #     DCForm1.Series23.Clear()
    #     DCForm1.Series24.Clear()
    #     DCForm1.Series23.Pointer.HorizSize = int(0.9 * DCForm1.Chart9.Width / US.Nu_steps / 2)
    #     DCForm1.Series23.Pointer.VertSize = int(0.8 * DCForm1.Chart9.Height / US.Ns_steps / 2)

    i_step_ = 0
    if not IfForward:
        i_step_ = globs.N_steps + 1

    while (i_step_ <= globs.N_steps and IfForward) or (i_step_ >= 1 and (IfForward==False)):
        i_step_ += 1
        if not IfForward:
            i_step_ -= 2

        globs.i_step = i_step_  #i_step_Shuffled[i_step_]
        # **************
        Measure(DCForm1)
        # **************
        # Drawing
        DCForm1.DrawArrays()

        # if globs.Protocol == 3:  # Tongue
        #     iu_step_, is_step_ = US_Define_iu_is_from_i_step(globs.i_step)
        #     DCForm1.Series23.AddXY(iu_step_, is_step_)

        # Writing
        Writing.WriteTraceInFile(DCForm1)
        # Analysis
        AnalyseTrace(DCForm1)
        # Remember Results of the Trial
        if i_step_<=200:
            globs.ResultsOfSeries[i_step_].PP = copy.deepcopy(globs.PP)
            globs.ResultsOfSeries[i_step_].EC = copy.deepcopy(globs.EC)
            globs.ResultsOfSeries[i_step_].CC = copy.deepcopy(globs.CC)
        DCForm1.root.update()

    #DCForm1.Button1.configure(state="normal") #Enabled


#=======================================================================================================================
def VisibleInvisible_Spin(SpinBox,TrueFalse):
    if TrueFalse :
        SpinBox.grid()
    else:
        SpinBox.grid_remove()

def VisibleInvisible_Label(Label,TrueFalse):
    if TrueFalse :
        label.config(state="normal")
    else:
        label.config(state="hidden")

def RunPatching(DCForm1):
    DCForm9 = DCForm1.DCForm9
    # VC parameters
    VisibleInvisible_Spin(DCForm9.spin_64, (not globs.Protocol == 5))
    # DCForm9.Label65.Visible = not globs.Protocol == 5
    # DCForm9.Label75.Visible = globs.Protocol == 5
    # DCForm9.Label89.Visible = globs.Protocol == 5
    # DCForm9.Label94.Visible = globs.Protocol == 5
    # DCForm9.Label2.Visible = globs.Protocol == 5
    # DCForm9.Label74.Visible = globs.Protocol == 5
    # DCForm9.Label106.Visible = globs.Protocol == 5
    VisibleInvisible_Spin(DCForm9.spin_74, (globs.Protocol == 5))
    VisibleInvisible_Spin(DCForm9.spin_85, (globs.Protocol == 5))
    VisibleInvisible_Spin(DCForm9.spin_87, (globs.Protocol == 5))
    VisibleInvisible_Spin(DCForm9.spin_2, (globs.Protocol == 5))
    VisibleInvisible_Spin(DCForm9.spin_73, (globs.Protocol == 5))
    VisibleInvisible_Spin(DCForm9.spin_100, (globs.Protocol == 5))
    # -----------
    DCForm1.Panel1.Color = 0x002626FB
    globs.i_step = 0
    while globs.i_step < float(DCForm9.spin_1.get()) and DCForm1.Button11.cget("text") != 'Patching':
        globs.i_step += 1
        # **************
        Measure(DCForm1)
        # **************
        DCForm1.Panel1.create_rectangle(25, 10, 80, 60, fill="yellow")
        DCForm1.root.update()

        # Drawing
        DCForm1.CleanPlots()
        DCForm1.DrawArrays()
        # DiscontinuousAxisMaximum(DCForm1.Chart1.LeftAxis)
        # Estimate G
        if globs.Protocol == 5:
            DCForm9.spin_87.Font.Color = clred
            DCForm9.spin_2.Font.Color = clred
            DCForm9.spin_73.Font.Color = clRed
            DCForm9.spin_100.Font.Color = clRed

    # Set panel color to clMoneyGreen and Button11 caption to "Patching"
    DCForm1.Panel1.create_rectangle(25, 10, 80, 60, fill="green")
    DCForm1.Button11.config(text = 'Patching')
    DCForm9.Form9Hide()


def AnalyseTrace(DCForm1):
    from PP_Analysis import AnalysePassive
    match globs.Protocol:
        case 0:  # Background
            pass
        case 1:  # Current-Clamp
            pass
        case 2:  # PassiveProperties
            AnalysePassive(DCForm1)
        case 3:  # Tongue
            pass
        case 5:  # Dynamic VC
            pass
        case 6:
            pass


def Define_I_G(time_,dt_):
    # Define I and G based on Protocol
    I, G, C, gNMDA = 0,0,0,0
    match globs.Protocol:
        case 0:  # Background
            pass
        case 1:  # Current-Clamp
            I, G, C, gNMDA = CurrentClamp.CC_Define_I_G(time_)
        case 2:  # PassiveProperties
            I, G, C = Passive.PP_Define_I_G(time_)
        case 3:  # Tongue
            pass
        case 5:  # Dynamic VC
            pass
        case 6:
            I = MathNeuro.MN_Define_I(time_,dt_)
    return I, G, C, gNMDA



