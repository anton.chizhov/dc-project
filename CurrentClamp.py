# unit CurrentClamp;
# {
#  The dynamic current-clamp protocols that are running in real-time
#  are described here.
#  The command current Icom is calculated by knowing Vcom as follows (line 64):
#  Icom :=  I_ - G_*(Vcom-Vus) - C_* d Vcom /dt
#  where the I_, G_ and C_ can be set as constants or functions.
# }

import math
import globs

def ReadCurrenClampParameters():
    globs.N_steps = 1
    if globs.CC.T_I == 0 and globs.CC.TypeOfInputIndex in range(1, 5):
        globs.CC.TypeOfInputIndex = 0
    if globs.CC.T_g == 0 and globs.CC.TypeOfInputIndex in range(2, 4):
        globs.CC.TypeOfInputIndex = 0


def CC_Define_I_G(time_):
    I = 0
    G = 0
    C = 0
    gNMDA = 0
    if (globs.CC.IfStimWholeTime == 1) or (time_ >= globs.t_StartStim and time_ < globs.t_EndStim):
        if globs.CC.TypeOfInputIndex == 0:  # 'Constant'
            I = globs.CC.I_stim
            G = globs.CC.g_stim
            C = globs.CC.C_stim
        elif globs.CC.TypeOfInputIndex == 1:  # 'I-sin, g-const'
            I = globs.CC.I_stim * math.sin(2 * math.pi / globs.CC.T_I * time_)
            G = globs.CC.g_stim
        elif globs.CC.TypeOfInputIndex == 2:  # 'I-const, g-cos'
            I = globs.CC.I_stim
            G = globs.CC.g_stim * (1 + math.cos(2 * math.pi / globs.CC.T_g * time_))
        elif globs.CC.TypeOfInputIndex == 3:  # 'I-sin, g-cos'
            I = globs.CC.I_stim * math.sin(2 * math.pi / globs.CC.T_I * time_)
            G = globs.CC.g_stim * (1 + math.cos(2 * math.pi / globs.CC.T_g * time_))
        elif globs.CC.TypeOfInputIndex == 4:  # 'I-steps, g-const'
            if math.sin(2 * math.pi / globs.CC.T_I * time_) < 0:
                I = globs.CC.I_stim
            else:
                I = 0
            G = globs.CC.g_stim
    return I, G, C, gNMDA


def StimCurrent_CC(time_, V_, Vprev_):
    I_, G_, C_, gNMDA_ = CC_Define_I_G(time_)
    if abs(globs.V) < 150:
        return I_ - G_ * (V_ - globs.Vus) - C_ * (V_ - Vprev_) / globs.AcqTime
    else:
        return 0


def RealTime_CurrentClamp():
    V_ = globs.V
    # Delay V
    if globs.EC.NstepsOfDelayV > 0:
        for i in range(globs.EC.NstepsOfDelayV - 1, -1, -1):
            globs.EC.V_delayed[i + 1] = globs.EC.V_delayed[i]
        globs.EC.V_delayed[0] = globs.V
        V_ = globs.EC.V_delayed[globs.EC.NstepsOfDelayV]
    globs.Icom = StimCurrent_CC(globs.time, V_, globs.Vprev)

