# unit OnlineComp;
# {
#   The pipette resistance and voltage offset are compensated in real-time.
#   Recorded voltage V is filtered and compensated depending on
#   actually injected current Ireal, which is also preliminary filtered.
# }

class ElectrodeCompensation:
    def __init__(self):
        self.G1 = 0  # electrode conductance
        self.Ireal_prev = 0
        self.Ireal_pprev = 0
        self.tau_Vfilt = 0  # ms
        self.tau_Ifilt = 0  # ms
        self.I_filt = 0
        self.V_filt = 0
        self.Ireal_Or_Icom = 0
        self.NstepsOfDelayV = 0
        self.V_delayed = []

EC = ElectrodeCompensation()

def ReadElectrodeCompensationParameters():
    EC.G1 = DCForm1.DDSpinEdit56.Value  # nS, electrode conductance
    EC.Ireal_prev = 0
    EC.Ireal_pprev = 0
    EC.tau_Vfilt = DCForm1.DDSpinEdit57.Value  # ms
    EC.tau_Ifilt = DCForm1.DDSpinEdit58.Value  # ms
    EC.I_filt = 0
    EC.V_filt = Vus
    EC.Ireal_Or_Icom = DCForm1.ComboBox7.ItemIndex + 1
    EC.NstepsOfDelayV = int(DCForm1.DDSpinEdit67.Value)
    EC.V_delayed = [0] * (EC.NstepsOfDelayV + 1)

def RealTime_ElectrodeCompensation():
    global V, Ireal, Icom, AcqTime

    # Filter V
    if EC.tau_Vfilt > 0:
        EC.V_filt += AcqTime / EC.tau_Vfilt * (V - EC.V_filt)
        V = EC.V_filt

    # Compensate Gel
    if EC.G1 == 0:
        return

    if EC.Ireal_Or_Icom == 1:
        I_ = Ireal
    elif EC.Ireal_Or_Icom == 2:
        I_ = Icom

    # Filter Ireal
    if EC.tau_Ifilt > 0:
        EC.I_filt += AcqTime / EC.tau_Ifilt * (I_ - EC.I_filt)
        dVseries_ = EC.I_filt / EC.G1
    else:
        dVseries_ = EC.Ireal_prev / EC.G1

    if abs(dVseries_) < 0.05:  # 50 mV
        # Voltage correction
        V -= dVseries_

    EC.Ireal_pprev = EC.Ireal_prev
    EC.Ireal_prev = I_



