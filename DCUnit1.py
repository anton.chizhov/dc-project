import tkinter as tk
from tkinter import ttk, Frame
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from tkinter import Spinbox, LabelFrame, Label, colorchooser
from datetime import date
import time

import Measurement
import globs
import FileDialog
import Acquisition
import PP_Analysis
from DCUnit9 import TDCForm9

from PyDAQmx.DAQmxFunctions import *
from PyDAQmx import *



#***************************
class TDCForm1(tk.Toplevel):
# ***************************
    """
      GUI, main form - DCForm1
    """

    def do_something(self):
        print("Doing something...")

    def select_radiobutton(self,radio_btn, radio_buttons):
        radio_btn.select()
        for btn in radio_buttons:
            if btn != radio_btn:
                btn.deselect()

    def set_BasicColor(self):
        globs.BasicColor = colorchooser.askcolor()[1]
        self.root.configure(bg=globs.BasicColor)

    def quit_application(self):
        self.root.quit()

    def Moveallframesinlefttopcorner1Click(self):
        self.Left = 0
        self.Top = 0
        #DCForm9.Left := 0;  DCForm9.Top := 0;

    def Sketchview1Click(self):
        if self.Sketchview1_Checked:
            self.DDSpinEdit27.set(1)  # dt_draw
        else:
            self.DDSpinEdit27.set(0)


    def __init__(self):
        # super().__init__()
        globs.BasicColor = '#D6E2E2' #F0C8A4'
        self.root = tk.Tk()
        self.root.configure(bg=globs.BasicColor)
        self.root.title('DC-Project, v.23.6')
        self.root.tk.call('tk', 'scaling', 1.5)

        # create a custom style with a light blue background color
        self.style = ttk.Style()
        self.style.configure('My.TFrame', background='#E6F2F2')
        self.style.configure("My.TLabelframe", background="#E6F2F2")
        self.style.configure("My.TLabel", background="#E6F2F2")

        # Main Menu ********************************************************************************************
        self.menu_bar = tk.Menu(self.root)

        # Create the File menu
        file_menu = tk.Menu(self.menu_bar, tearoff=0)
        file_menu.add_command(label="Open", command=self.do_something)
        file_menu.add_command(label="Save", command=self.do_something)
        file_menu.add_command(label="Set color", command=self.set_BasicColor)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.quit_application)
        self.menu_bar.add_cascade(label="File", menu=file_menu)

        # Create the Options menu
        options_menu = tk.Menu(self.menu_bar, tearoff=0)
        options_menu.add_command(label="Move all frames in left top corner", command=self.Moveallframesinlefttopcorner1Click)
        options_menu.add_command(label="Activate all curves", command=self.do_something)
        self.Sketchview1_Checked= tk.BooleanVar()
        self.Sketchview1_Checked.set(False)
        options_menu.add_checkbutton(label="Sketch view", variable=self.Sketchview1_Checked, command=self.Sketchview1Click)
        self.menu_bar.add_cascade(label="Edit", menu=options_menu)

        # Attach the menu bar to the main window
        self.root.config(menu=self.menu_bar)

        # Main Frame *******************************************************************************************

        self.frame = ttk.Frame(self.root, style="My.TFrame")
        self.frame.pack(pady=10)
        #self.frame.configure(bg='#C8F0A4')  # D6E2E2')

        # Frame\groupbox "Buttons etc."
        self.groupbox1 = ttk.LabelFrame(self.frame, text="Buttons etc.", style="My.TLabelframe")
        self.groupbox1.pack(side=tk.LEFT, padx=10)
        # Add a green rectangle under the label
        self.Panel1 = tk.Canvas(self.groupbox1, width=100, height=70, bg=globs.BasicColor)
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="green")
        self.Panel1.grid(row=0, column=0, padx=0, pady=0, sticky='ew')
        # Label
        self.Label27 = ttk.Label(self.groupbox1, text="Trace number", style="My.TLabel")
        self.Label27.grid(row=1, column=0, padx=5, pady=1, sticky='ew')

        # Buttons
        self.Button1 = tk.Button(self.groupbox1, text=" Run Once ", font=("Arial", 12), command=self.on_Button1_click)
        self.Button1.grid(row=0, column=1, padx=5, pady=1, sticky='ew')
        self.Button2 = tk.Button(self.groupbox1, text="Stop", font=("Arial", 12), command=self.on_Button2_click)
        self.Button2.grid(row=0, column=2, padx=5, pady=1, sticky='ew')
        self.Button11 = tk.Button(self.groupbox1, text="Patching", command=self.on_Button11_click)
        self.Button11.grid(row=1, column=1, padx=5, pady=1, sticky='ew')
        self.Button6 = tk.Button(self.groupbox1, text="Clean", command=self.CleanPlots)
        self.Button6.grid(row=1, column=2, padx=5, pady=1, sticky='ew')

        # Frame\Stimulus
        self.groupbox3 = ttk.LabelFrame(self.frame, text="Stimulus", style="My.TLabelframe")
        self.groupbox3.pack(side=tk.LEFT, padx=10)
        self.Label1 = ttk.Label(self.groupbox3, text='I_stim, pA', style="My.TLabel")
        self.Label1.grid(row=0, column=0, padx=5, pady=1, sticky='ew')
        self.DDSpinEdit1 = ttk.Spinbox(self.groupbox3, font=tk.font.Font(size=14), command=self.on_DDSpinEdit1_click, foreground="red", from_=-10000, to=10000, increment=10, width=10)
        self.DDSpinEdit1.insert(0,"0")
        self.DDSpinEdit1.grid(row=0, column=1, padx=5, pady=1, sticky='ew')
        self.Label2 = ttk.Label(self.groupbox3, text='g_stim, nS', style="My.TLabel")
        self.Label2.grid(row=1, column=0, padx=5, pady=4, sticky='ew')
        self.DDSpinEdit37 = ttk.Spinbox(self.groupbox3, font=tk.font.Font(size=14), command=self.on_DDSpinEdit37_click, from_=-100, to=100, increment=0.1, width=10)
        self.DDSpinEdit37.insert(0,"0")
        self.DDSpinEdit37.grid(row=1, column=1, padx=5, pady=4, sticky='ew')

        # Frame\GroupBox Protocols
        self.RadioGroup2 = ttk.LabelFrame(self.frame, text="Protocols", style="My.TLabelframe")
        self.RadioGroup2.pack(side=tk.LEFT, padx=10)
        self.RadioGroup2_value = tk.IntVar()
        self.item1_1 = tk.Radiobutton(self.RadioGroup2, text="BackGround",          variable=self.RadioGroup2_value, value=0)
        self.item1_1.pack(pady=0, anchor='w')
        self.item1_2 = tk.Radiobutton(self.RadioGroup2, text="Current-Clamp (CC)",  variable=self.RadioGroup2_value, value=1)
        self.item1_2.pack(pady=0, anchor='w')
        self.item1_3 = tk.Radiobutton(self.RadioGroup2, text="Passive Properties",  variable=self.RadioGroup2_value, value=2)
        self.item1_3.pack(pady=0, anchor='w')
        self.item1_4 = tk.Radiobutton(self.RadioGroup2, text="Tongue",              variable=self.RadioGroup2_value, value=3)
        self.item1_4.pack(pady=0, anchor='w')
        self.item1_5 = tk.Radiobutton(self.RadioGroup2, text="Voltage-Clamp (VC)",  variable=self.RadioGroup2_value, value=5)
        self.item1_5.pack(pady=0, anchor='w')
        self.item1_6 = tk.Radiobutton(self.RadioGroup2, text="MathNeuro",           variable=self.RadioGroup2_value, value=6)
        self.item1_6.pack(pady=3, anchor='w')
        radio_buttons = [self.item1_1,self.item1_2,self.item1_3,self.item1_4,self.item1_5,self.item1_6]
        self.RadioGroup2_value.set(1)
        self.select_radiobutton(self.item1_2,radio_buttons)

        # Frame\GroupBox SubProtocols
        self.GroupBox21 = ttk.LabelFrame(self.frame, text="SubProtocols", style="My.TLabelframe")
        self.GroupBox21.pack(side=tk.LEFT, padx=10)
        self.CheckBox5_Checked = tk.IntVar(value=0)
        self.CheckBox5 = ttk.Checkbutton(self.GroupBox21, text="Filter V, compensate Gel", variable=self.CheckBox5_Checked).pack(pady=1, anchor='w')
        self.CheckBox14_Checked = tk.IntVar(value=0)
        self.CheckBox14 = ttk.Checkbutton(self.GroupBox21, text="Noise", variable=self.CheckBox14_Checked).pack(pady=1, anchor='w')

        # Frame\Folders
        self.Folders = FileDialog.FolderExplorer(self, master=self.frame)
        self.Folders.pack(side=tk.RIGHT, padx=10)

        # Tabs *************************************************************************
        self.notebook = ttk.Notebook(self.root)
        self.notebook.pack(fill='both', expand=True)
        #style.configure("TNotebook.Tab", font=("Arial", 12))

        self.tab1 = ttk.Frame(self.notebook, style='My.TFrame')
        self.notebook.add(self.tab1, text='  Records  ')
        self.tab2 = ttk.Frame(self.notebook, style='My.TFrame')
        self.notebook.add(self.tab2, text='  Options  ')
        self.tab3 = ttk.Frame(self.notebook, style='My.TFrame')
        self.notebook.add(self.tab3, text='  Current-Clamp  ')
        self.notebook.select(self.tab2)
        self.tab4 = ttk.Frame(self.notebook, style='My.TFrame')
        self.notebook.add(self.tab4, text='  MathNeuro  ')
        self.tab5 = ttk.Frame(self.notebook, style='My.TFrame')
        self.notebook.add(self.tab5, text='  PassiveProperties  ')



        # Tab1 - Records
        self.frame_tab1 = ttk.Frame(self.tab1, style="My.TFrame")
        self.frame_tab1.pack(pady=0)
        #self.frame_tab1.configure(bg='#C8F0A4')  # D6E2E2')

        self.Label57 = ttk.Label(self.frame_tab1, text="Electrode\' G1, nS", style="My.TLabel")
        self.Label57.grid(row=0, column=0, padx=5, pady=5, sticky='ew')
        self.DDSpinEdit56 = ttk.Spinbox(self.frame_tab1, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit56.insert(0,"0")
        self.DDSpinEdit56.grid(row=0, column=1, padx=5, pady=1, sticky='ew')
        self.Label5 = ttk.Label(self.frame_tab1, text='Control time = ', style="My.TLabel")
        self.Label5.grid(row=0, column=2, padx=50, pady=5, sticky='ew')
        self.CheckBox4_Checked = tk.BooleanVar(value=True)
        self.CheckBox4 = ttk.Checkbutton(self.frame_tab1, text='Show command I and G', variable=self.CheckBox4_Checked)
        self.CheckBox4.grid(row=0, column=3, padx=5, pady=5, sticky='w')

        # Tab2 - Options

        # Tab2/Time
        self.group_box2 = ttk.LabelFrame(self.tab2, text='Time', style="My.TLabelframe")
        self.group_box2.grid(row=1, column=0, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box2, text='T_end', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit3 = ttk.Spinbox(self.group_box2, from_=0, to=300000, increment=500, width=10)
        self.DDSpinEdit3.insert(0,"1000")
        self.DDSpinEdit3.grid(row=0, column=1, padx=5, pady=0)
        ttk.Label(self.group_box2, text='AcqTime, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit4 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=0.01, width=10)
        self.DDSpinEdit4.insert(0,"0.030")
        self.DDSpinEdit4.grid(row=1, column=1, padx=5, pady=0)
        ttk.Label(self.group_box2, text='t_StartStim, ms', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit9 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=100, width=10)
        self.DDSpinEdit9.insert(0,"100")
        self.DDSpinEdit9.grid(row=2, column=1, padx=5, pady=0)
        ttk.Label(self.group_box2, text='t_EndStim, ms', style="My.TLabel").grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit10 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=100, width=10)
        self.DDSpinEdit10.insert(0,"600")
        self.DDSpinEdit10.grid(row=3, column=1, padx=5, pady=0)
        self.CheckBox18_Checked = tk.BooleanVar(value=False)
        self.CheckBox18 = ttk.Checkbutton(self.group_box2, text='IfStimWholeTime', variable=self.CheckBox18_Checked).grid(row=4, column=0, padx=5, pady=5, sticky='w')

        # Tab2/Groupbox Scales
        self.group_box3 = ttk.LabelFrame(self.tab2, text='Scales', style="My.TLabelframe")
        self.group_box3.grid(row=1, column=1, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box3, text='V_scale, mV/V', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit25 = ttk.Spinbox(self.group_box3, from_=0, to=10000, increment=50, width=10)
        self.DDSpinEdit25.insert(0,"0")
        self.DDSpinEdit25.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box3, text='Icom_scale, pA/V', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit26 = ttk.Spinbox(self.group_box3, from_=0, to=10000, increment=50, width=10)
        self.DDSpinEdit26.insert(0,"0")
        self.DDSpinEdit26.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.group_box3, text='Ireal_scale, pA/V', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit31 = ttk.Spinbox(self.group_box3, from_=50, to=10000, increment=50, width=10)
        self.DDSpinEdit31.insert(0,"0")
        self.DDSpinEdit31.grid(row=2, column=1, padx=5, pady=5)
        ttk.Label(self.group_box3, text='V_offset, mV', style="My.TLabel").grid(row=0, column=2, padx=5, pady=5, sticky='w')
        self.DDSpinEdit39 = ttk.Spinbox(self.group_box3, from_=-1000, to=1000, increment=1, width=10)
        self.DDSpinEdit39.insert(0,"0")
        self.DDSpinEdit39.grid(row=0, column=3, padx=5, pady=5)
        ttk.Label(self.group_box3, text='Icom_offset, pA', style="My.TLabel").grid(row=1, column=2, padx=5, pady=5, sticky='w')
        self.DDSpinEdit38 = ttk.Spinbox(self.group_box3, from_=-1000, to=1000, increment=1, width=10)
        self.DDSpinEdit38.insert(0,"0")
        self.DDSpinEdit38.grid(row=1, column=3, padx=5, pady=5)
        ttk.Label(self.group_box3, text='Ireal_offset, pA', style="My.TLabel").grid(row=2, column=2, padx=5, pady=5, sticky='w')
        self.DDSpinEdit40 = ttk.Spinbox(self.group_box3, from_=-1000, to=1000, increment=1, width=10)
        self.DDSpinEdit40.insert(0,"0")
        self.DDSpinEdit40.grid(row=2, column=3, padx=5, pady=5)

        # Tab2/Groupbox Vus
        self.group_box4 = ttk.LabelFrame(self.tab2, text='Vus', style="My.TLabelframe")
        self.group_box4.grid(row=1, column=2, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box4, text='Vus', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit30 = ttk.Spinbox(self.group_box4, from_=0, to=1000, increment=5, width=10)
        self.DDSpinEdit30.insert(0,"-70")
        self.DDSpinEdit30.grid(row=0, column=1, padx=5, pady=0)

        # Tab2/Groupbox Show curves
        self.group_box5 = ttk.LabelFrame(self.tab2, text='Show curves', style="My.TLabelframe")
        self.group_box5.grid(row=1, column=3, padx=5, pady=5, sticky='ew')
        self.CheckBox2_Checked = tk.BooleanVar(value=True)
        self.CheckBox2 = ttk.Checkbutton(self.group_box5, text='V', variable=self.CheckBox2_Checked).grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.CheckBox1_Checked = tk.BooleanVar(value=True)
        self.CheckBox1 = ttk.Checkbutton(self.group_box5, text='Icom', variable=self.CheckBox1_Checked).grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.CheckBox3_Checked = tk.BooleanVar(value=False)
        self.CheckBox3 = ttk.Checkbutton(self.group_box5, text='Ireal', variable=self.CheckBox3_Checked).grid(row=3, column=0, padx=5, pady=5, sticky='w')

        # Tab2/Groupbox Noise
        self.group_box6 = ttk.LabelFrame(self.tab2, text='Noise', style="My.TLabelframe")
        self.group_box6.grid(row=2, column=1, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box6, text='tau_I, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit68 = ttk.Spinbox(self.group_box6, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit68.insert(0,"3")
        self.DDSpinEdit68.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box6, text='tau_G, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit69 = ttk.Spinbox(self.group_box6, from_=0, to=1000, increment=1, width=10)
        self.DDSpinEdit69.insert(0,"3")
        self.DDSpinEdit69.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.group_box6, text='sgm_I, pA', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit70 = ttk.Spinbox(self.group_box6, from_=-1000, to=1000, increment=1, width=10)
        self.DDSpinEdit70.insert(0,"10")
        self.DDSpinEdit70.grid(row=2, column=1, padx=5, pady=5)
        ttk.Label(self.group_box6, text='sgm_G, nS', style="My.TLabel").grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit71 = ttk.Spinbox(self.group_box6, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit71.insert(0,"0")
        self.DDSpinEdit71.grid(row=3, column=1, padx=5, pady=5)

        # Tab2/Groupbox Defaults
        self.RadioGroup1 = ttk.LabelFrame(self.tab2, text="Defaults", style="My.TLabelframe")
        self.RadioGroup1.grid(row=2, column=0, padx=5, pady=5, sticky='ew')
        self.RadioGroup1_value = tk.IntVar()
        self.item11_1 = tk.Radiobutton(self.RadioGroup1, text="Real neuron", variable=self.RadioGroup1_value, value=0, command=self.SetDefaults)
        self.item11_1.pack(pady=1, anchor='w')
        self.item11_2 = tk.Radiobutton(self.RadioGroup1, text="Amplifier model cell", variable=self.RadioGroup1_value, value=1, command=self.SetDefaults)
        self.item11_2.pack(pady=1, anchor='w')
        self.item11_3 = tk.Radiobutton(self.RadioGroup1, text="Arduino", variable=self.RadioGroup1_value, value=2, command=self.SetDefaults)
        self.item11_3.pack(pady=1, anchor='w')
        self.item11_4 = tk.Radiobutton(self.RadioGroup1, text="Charangeet", variable=self.RadioGroup1_value, value=3, command=self.SetDefaults)
        self.item11_4.pack(pady=1, anchor='w')
        radio_buttons = [self.item11_1,self.item11_2,self.item11_3,self.item11_4]
        self.RadioGroup1_value.set(2)
        self.select_radiobutton(self.item11_3,radio_buttons)


        # Tab2/Groupbox Drawing
        self.group_box7 = ttk.LabelFrame(self.tab2, text='Drawing', style="My.TLabelframe")
        self.group_box7.grid(row=2, column=2, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box7, text='dt_draw, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit27 = ttk.Spinbox(self.group_box7, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit27.insert(0,"3")
        self.DDSpinEdit27.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box7, text='Epoch, s', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit28 = ttk.Spinbox(self.group_box7, from_=-150, to=100, increment=1, width=10)
        self.DDSpinEdit28.insert(0,"3")
        self.DDSpinEdit28.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.group_box7, text='dt_AcqForm, ms', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit112 = ttk.Spinbox(self.group_box7, from_=-150, to=100, increment=0.1, width=10)
        self.DDSpinEdit112.insert(0,"0")
        self.DDSpinEdit112.grid(row=2, column=1, padx=5, pady=5)
        ttk.Label(self.group_box7, text='Skip initial interval, s', style="My.TLabel").grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit158 = ttk.Spinbox(self.group_box7, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit158.insert(0,"0")
        self.DDSpinEdit158.grid(row=3, column=1, padx=5, pady=5)

        # Tab2/Groupbox Filter and comp Gel
        self.group_box7 = ttk.LabelFrame(self.tab2, text='Filter and comp Gel', style="My.TLabelframe")
        self.group_box7.grid(row=2, column=3, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box7, text='V: tau_Vfilt, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit57 = ttk.Spinbox(self.group_box7, from_=0, to=1000, increment=1, width=10)
        self.DDSpinEdit57.insert(0,"3")
        self.DDSpinEdit57.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box7, text='Ireal: tau_Ifilt, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit58 = ttk.Spinbox(self.group_box7, from_=-150, to=100, increment=1, width=10)
        self.DDSpinEdit58.insert(0,"3")
        self.DDSpinEdit58.grid(row=1, column=1, padx=5, pady=5)
        self.ComboBox7 = ttk.Combobox(self.group_box7, values=["Compensate G1 by Ireal","Compensate G1 by Icom","No compensation"])
        self.ComboBox7.grid(row=2, column=0, padx=5, pady=5)
        self.ComboBox7.current(2)
        ttk.Label(self.group_box7, text='NstepsOfDelay', style="My.TLabel").grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit67 = ttk.Spinbox(self.group_box7, from_=0, to=1000, increment=1, width=10)
        self.DDSpinEdit67.insert(0,"0")
        self.DDSpinEdit67.grid(row=3, column=1, padx=5, pady=5)

        # Tab2/Groupbox Acquisition range checking
        self.group_box8 = ttk.LabelFrame(self.tab2, text='Acquisition range checking', style="My.TLabelframe")
        self.group_box8.grid(row=3, column=0, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box8, text='Vmin, mV', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit80 = ttk.Spinbox(self.group_box8, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit80.insert(0,"-150")
        self.DDSpinEdit80.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box8, text='Vmax, mV', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit81 = ttk.Spinbox(self.group_box8, from_=-150, to=100, increment=10, width=10)
        self.DDSpinEdit81.insert(0,"100")
        self.DDSpinEdit81.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.group_box8, text='dVmax, mV', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit82 = ttk.Spinbox(self.group_box8, from_=-150, to=100, increment=1, width=10)
        self.DDSpinEdit82.insert(0,"20")
        self.DDSpinEdit82.grid(row=2, column=1, padx=5, pady=5)

        # Tab2/Groupbox Control options
        self.group_box9 = ttk.LabelFrame(self.tab2, text='Control options', style="My.TLabelframe")
        self.group_box9.grid(row=3, column=1, padx=5, pady=5, sticky='ew')
        self.CheckBox9_Checked = tk.BooleanVar(value=False)
        self.checkbox_9 = ttk.Checkbutton(self.group_box9, text='Write in files', variable=self.CheckBox9_Checked)
        self.checkbox_9.grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.ComboBox8 = ttk.Combobox(self.group_box9,
                                   values=["Binary and text", "Only text-file", "Only binary file"])
        self.ComboBox8.current(2)
        self.ComboBox8.grid(row=1, column=1, padx=5, pady=5)
        self.var_10 = tk.BooleanVar(value=False)
        self.checkbox_2 = ttk.Checkbutton(self.group_box9, text='No plots in Background mode', variable=self.var_10)
        self.checkbox_2.grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.CheckBox12_Checked = tk.BooleanVar(value=False)
        self.CheckBox12 = ttk.Checkbutton(self.group_box9, text='Read parameters from file', variable=self.CheckBox12_Checked)
        self.CheckBox12.grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.ComboBox2 = ttk.Combobox(self.group_box9,
                                   values=["No DAQ-card (simulation)","Device number 1","Device number 2","Device number 3","Device number 4","Device number 5"])
        self.ComboBox2.current(0)   # DeviceNumber
        self.ComboBox2.bind('<Double-Button-1>', self.on_ComboBox2_double_click())
        self.ComboBox2.grid(row=4, column=0, padx=5, pady=5)
        ttk.Label(self.group_box9, text='n_RememberInArr', style="My.TLabel").grid(row=5, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit142 = ttk.Spinbox(self.group_box9, from_=0, to=1000, increment=0.1, width=10)
        self.DDSpinEdit142.insert(0,"1")
        self.DDSpinEdit142.grid(row=5, column=1, padx=5, pady=0)

        # Tab2/SubjectiveNote
        self.SubjectiveNote = tk.Text(self.tab2, height=1, width=50)
        self.SubjectiveNote.grid(row=4, column=1, padx=5, pady=0)

        # Tab3: Current-Clamp

        # Tab3/Type of stimulation
        self.ComboBox1 = ttk.Combobox(self.tab3, width = 47, values=["Icom=I_stim - g_stim*(V-Vus) - C dV/dt","Icom=I_stim * sin(2 pi/ T_I t)  - g_stim*(V-Vus)","Icom=I_stim - g_stim*(1+cos(2 pi/T_g t))*(V-Vus)","Icom=I_stim* sin(2 pi/ T_I t) - g_stim*(1+cos(2 pi/T_g t))*(V-Vus)","Icom: I-steps, g-const"])
        self.ComboBox1.current(4)
        self.ComboBox1.grid(row=1, column=0, padx=5, pady=5)

        # Tab3/Groupbox Stimulation through  I, G and C
        self.group_box1 = ttk.LabelFrame(self.tab3, text='Stimulation through  I, G and C', style="My.TLabelframe")
        self.group_box1.grid(row=2, column=0, padx=5, pady=5, sticky='ew')
        ttk.Label(self.group_box1, text='I_stim is on top panel,  T_I, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit2 = ttk.Spinbox(self.group_box1, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit2.insert(0,"0")
        self.DDSpinEdit2.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.group_box1, text='g_stim is on top panel,  T_g, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit52 = ttk.Spinbox(self.group_box1, from_=-150, to=100, increment=10, width=10)
        self.DDSpinEdit52.insert(0,"0")
        self.DDSpinEdit52.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.group_box1, text='C=tau*GL, pF', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit53 = ttk.Spinbox(self.group_box1, from_=-150, to=100, increment=1, width=10)
        self.DDSpinEdit53.insert(0,"0")
        self.DDSpinEdit53.grid(row=2, column=1, padx=5, pady=5)


        # Tab4: MathNeuro **********************************************************************************************

        # Tab4/Groupbox Parameters of resonator-oscillator transfer current
        self.GroupBox44 = ttk.LabelFrame(self.tab4, text='Parameters of resonator-oscillator transfer current', style="My.TLabelframe")
        self.GroupBox44.grid(row=0, column=0, padx=5, pady=5, sticky='ew')
        ttk.Label(self.GroupBox44, text='I0, pA', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit165 = ttk.Spinbox(self.GroupBox44, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit165.insert(0,"30")
        self.DDSpinEdit165.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox44, text='tau, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit166 = ttk.Spinbox(self.GroupBox44, from_=0, to=100, increment=10, width=10)
        self.DDSpinEdit166.insert(0,"100")
        self.DDSpinEdit166.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox44, text='alpha, nS', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit167 = ttk.Spinbox(self.GroupBox44, from_=-150, to=100, increment=1, width=10)
        self.DDSpinEdit167.insert(0,"0")
        self.DDSpinEdit167.grid(row=2, column=1, padx=5, pady=5)

        # Tab4/SubjectiveNote
        self.Memo6 = tk.Text(self.tab4, height=10, width=50)
        self.Memo6.insert(tk.END," The injected current Icom is governed by \n eqs. of voltage-dependent oscillator: \n \n tau * d Icom / dt = - J + alpha * V \n \n tau * dJ / dt = Icom - I0")
        self.Memo6.grid(row=1, column=0, padx=5, pady=0)

        # Tab4/Combobox: Type of modelled neuron
        ttk.Label(self.tab4, text=' Type of modelled neuron: ', style="My.TLabel").grid(row=0, column=1, padx=5, pady=5, sticky='ew')
        self.ComboBox44 = ttk.Combobox(self.tab4, values=["HH","NaK"])
        self.ComboBox44.grid(row=0, column=2, padx=5, pady=5, sticky='ew')
        self.ComboBox44.current(1)


        # Tab5: PassiveProperties **********************************************************************************************

        self.FrameForTab5 = ttk.LabelFrame(self.tab5, text='Range of currents', style="My.TLabelframe")
        self.FrameForTab5.grid(row=0, column=1, padx=50, pady=1, sticky='ew')


        self.Button20 = tk.Button(self.FrameForTab5, text=" Automatic estimation of G_L ", font=tk.font.Font(size=14), command=self.on_Button20_click)
        self.Button20.grid(row=0, column=0, padx=25, pady=25, sticky='ew')

        # Tab5/Groupbox PP parameters
        self.GroupBox4 = ttk.LabelFrame(self.FrameForTab5, text='Range of currents', style="My.TLabelframe")
        self.GroupBox4.grid(row=1, column=0, padx=25, pady=25, sticky='ew')
        ttk.Label(self.GroupBox4, text='I_min, pA', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit5 = ttk.Spinbox(self.GroupBox4, from_=-1000, to=1000, increment=10, width=10)
        self.DDSpinEdit5.insert(0,"-200")
        self.DDSpinEdit5.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox4, text='I_max, pA', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit6 = ttk.Spinbox(self.GroupBox4, from_=-150, to=1000, increment=10, width=10)
        self.DDSpinEdit6.insert(0,"200")
        self.DDSpinEdit6.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox4, text='N_steps', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit7 = ttk.Spinbox(self.GroupBox4, from_=0, to=100, increment=1, width=10)
        self.DDSpinEdit7.insert(0,"9")
        self.DDSpinEdit7.grid(row=2, column=1, padx=5, pady=5)

        self.GroupBox5 = ttk.LabelFrame(self.FrameForTab5, text='Intervals for analysis', style="My.TLabelframe")
        self.GroupBox5.grid(row=2, column=0, padx=25, pady=25, sticky='ew')
        ttk.Label(self.GroupBox5, text='t1_start, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit14 = ttk.Spinbox(self.GroupBox5, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit14.insert(0,"40")
        self.DDSpinEdit14.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox5, text='t1_end, ms', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit15 = ttk.Spinbox(self.GroupBox5, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit15.insert(0,"100")
        self.DDSpinEdit15.grid(row=1, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox5, text='t2_start, ms', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit16 = ttk.Spinbox(self.GroupBox5, from_=0, to=10000, increment=50, width=10)
        self.DDSpinEdit16.insert(0,"350")
        self.DDSpinEdit16.grid(row=2, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox5, text='t2_end, ms', style="My.TLabel").grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit17 = ttk.Spinbox(self.GroupBox5, from_=0, to=10000, increment=50, width=10)
        self.DDSpinEdit17.insert(0,"550")
        self.DDSpinEdit17.grid(row=2, column=1, padx=5, pady=5)

        self.GroupBox6 = ttk.LabelFrame(self.FrameForTab5, text='Fit G_L', style="My.TLabelframe")
        self.GroupBox6.grid(row=1, column=1, padx=25, pady=25, sticky='ew')
        ttk.Label(self.GroupBox6, text='G_L, nS', style="My.TLabel", font=tk.font.Font(size=14), foreground="red").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit18 = ttk.Spinbox(self.GroupBox6, format='%.2f', font=tk.font.Font(size=14), foreground="red", from_=-100, to=1000, increment=0.5, width=10, command=self.on_click_DDSpinEdit18)
        self.DDSpinEdit18.insert(0,"2.0")
        self.DDSpinEdit18.grid(row=0, column=1, padx=5, pady=5)

        self.GroupBox7 = ttk.LabelFrame(self.FrameForTab5, text='Fit tau_m', style="My.TLabelframe")
        self.GroupBox7.grid(row=2, column=1, padx=25, pady=25, sticky='ew')
        ttk.Label(self.GroupBox7, text='tau_m, ms', style="My.TLabel").grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit54 = ttk.Spinbox(self.GroupBox7, from_=0, to=1000, increment=10, width=10, command=self.on_click_DDSpinEdit54)
        self.DDSpinEdit54.insert(0,"20")
        self.DDSpinEdit54.grid(row=0, column=1, padx=5, pady=5)
        ttk.Label(self.GroupBox7, text='C, pF', style="My.TLabel").grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.DDSpinEdit130 = ttk.Spinbox(self.GroupBox7, from_=0, to=1000, increment=10, width=10)
        self.DDSpinEdit130.insert(0,"20")
        self.DDSpinEdit130.grid(row=1, column=1, padx=5, pady=5)


        # Plots ********************************************************************************************************
        self.initialize_drawing()

        # Defaults
        self.SetDefaults()
        Measurement.SetParametersFromForms(self)
        #Acquisition.DAQ_SingleCommand_AO0_AO1_AI0_AI1_AI2((globs.Protocol==5), 1000/globs.AcqTime, globs.DeviceNumber)
        #Set_Mode_VC_CC

        # Form for Patching ***
        self.DCForm9 = TDCForm9(self)
        self.DCForm9.root.title("DCForm9 :Patching")
        self.DCForm9.root.geometry("+1600+200")
        #**********************

        #************************
        #Measurement.RunOnce(self)
        #************************

        self.root.geometry("+{}+{}".format(5, 50))

#***********************************************************************************************************************
#***********************************************************************************************************************

    def SetDefaults(self):
        #    from DC_Project import self
        global iDrawLimit
        iDrawLimit = 448000
        a = self.RadioGroup1_value.get()
        match a:
            case 0:  # Defaults for real neuron
                self.ComboBox7.current(1)  # Compensate by Icom
                self.DDSpinEdit39.set(0)  # {mV} // V_offset
                self.DDSpinEdit38.set(0)  # {pA} // Icom_offset
                self.DDSpinEdit25.set(100)  # {mV/V} // V_scale
                self.DDSpinEdit26.set(200)  # {pA/V} // Icom_scale
                self.DDSpinEdit31.set(1000)  # {pA/V} // Ireal_scale
                self.DDSpinEdit1.set(20)  # {pA} // I_stim
                self.DDSpinEdit4.set(0.030)  # {ms} // AcqTime
                self.DDSpinEdit30.set(-60)  # {mV} // Vus
                self.CheckBox5_Checked.set(0)  # // Gel compensation
                self.DDSpinEdit27.set(0.1)  # {ms} // dt_draw
                self.DDSpinEdit58.set(0.035)  # {ms} // EC.tau_Ifilt
                self.ComboBox2.current(0)  # // Device number for DAQ-card is 1
                DCForm9.DDSpinEdit74.set(0)  # {mV} // Vh0
                DCForm9.DDSpinEdit85.set(-10)  # {mV} // Vh1
                iDrawLimit = 140000
            case 1:  # Defaults for Amplifier model cell
                self.DDSpinEdit39.set(0)  # {mV} // V_offset
                self.DDSpinEdit38.set(0)  # {pA} // Icom_offset
                self.DDSpinEdit25.set(200)  # {mV/V} // V_scale
                self.DDSpinEdit26.set(400)  # {pA/V} // Icom_scale
                self.DDSpinEdit31.set(2000)  # {pA/V} // Ireal_scale
                self.DDSpinEdit1.set(50)  # {pA} // I_stim
                self.DDSpinEdit4.set(0.030)  # {ms} // AcqTime
                self.DDSpinEdit27.set(0)  # {ms} // dt_draw
                self.DDSpinEdit30.set(0)  # {mV} // Vus
            case 2:  # Defaults for Arduino
                self.DDSpinEdit39.set(-100)  # {mV} // V_offset
                self.DDSpinEdit38.set(0)  # {pA} // Icom_offset
                self.DDSpinEdit25.set(50)  # {mV/V} // V_scale
                self.DDSpinEdit26.set(50)  # {pA/V} // Icom_scale
                self.DDSpinEdit31.set(50)  # {pA/V} // Ireal_scale
                self.DDSpinEdit1.set(30)  # {pA} // I_stim
                self.DDSpinEdit4.set(0.080)  # {ms} // AcqTime
                self.DDSpinEdit5.set( 15) # {pA} // PP.I_min
                self.DDSpinEdit6.set( 50) # {pA} // PP.I_max
                self.DDSpinEdit27.set(0.3) # {ms} // dt_draw
                self.DDSpinEdit30.set(-70)  # {mV} // Vus
                self.CheckBox5_Checked.set(0)  # // Gel compensation
                self.ComboBox2.current(1)  # // Device number for DAQ-card
                self.CheckBox3_Checked.set(False) # Show Ireal
                self.CheckBox9_Checked.set(True) # Write in files
            case 3:  # Charangeet
                self.DDSpinEdit39.set(-64)  # {mV} // V_offset
                self.DDSpinEdit38.set(0)  # {pA} // Icom_offset
                self.DDSpinEdit25.set(2000)  # {mV/V} // V_scale
                self.DDSpinEdit26.set(200)  # {pA/V} // Icom_scale
                self.DDSpinEdit31.set(200)  # {pA/V} // Ireal_scale
                self.DDSpinEdit1.set(50)  # {pA} // I_stim
                self.DDSpinEdit4.set(0.060)  # {ms} // AcqTime
                self.DDSpinEdit30.set(-70)  # {mV} // Vus
                self.CheckBox5_Checked.set(0)  # // Gel compensation
                self.DDSpinEdit27.set(0)  # {ms} // dt_draw
                self.DDSpinEdit58.set(0.035)  # {ms} // EC.tau_Ifilt

    def on_ComboBox2_double_click(self):
        self.ComboBox2.current(0)


    def on_DDSpinEdit1_click(self):
        globs.CC.I_stim = self.DDSpinEdit1.get()

    def on_DDSpinEdit37_click(self):
        globs.CC.g_stim = self.DDSpinEdit37.get()

    def on_click_DDSpinEdit18(self):  # G_L
        self.axPP[0].clear()
        PP_Analysis.DrawLineWithSlope_G(self)

    def on_click_DDSpinEdit54(self):  # tau_m
        self.axPP[1].clear()
        PP_Analysis.DrawExponentialFit_taum(self)

    def on_Button1_click(self):     # Run Once
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="red")
        #time.sleep(1)
        self.root.update()
        #********
        Measurement.RunOnce(self)
        #********
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="green")

    def on_Button2_click(self):     # Stop
        globs.i_end = 0
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="orange")
        self.root.update()
        time.sleep(5)
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="green")

    def on_Button4_click(self):     # Run Series
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="red")
        self.root.update()
        #*****************************************
        Measurement.RunSeries(self,IfForward=True)
        #*****************************************
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="green")

    def on_Button20_click(self):     # AutomaticEstimationOf_GL
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="red")
        self.root.update()
        #********
        PP_Analysis.AutomaticEstimationOf_GL(self)
        #********
        self.Panel1.create_rectangle(25, 10, 80, 60, fill="green")

    def on_Button11_click(self):     # Patching
        if self.Button11.cget("text") == 'Patching':
            self.Button11.config(fg="red")
            self.Button11.config(text='Stop Patching')
            # Show DCForm9
            self.DCForm9.root.deiconify()
            # Save the initial values of DCForm1's spinboxes
            self.DCForm9.x3 = self.DDSpinEdit3.get()
            self.DCForm9.x4 = self.DDSpinEdit4.get()
            self.DCForm9.x9 = self.DDSpinEdit9.get()
            self.DCForm9.x10 = self.DDSpinEdit10.get()
            self.DCForm9.x64 = self.DDSpinEdit1.get()
            # Set the values of DCForm1's spinboxes to the values of TDCForm9's spinboxes
            self.DDSpinEdit3.set(self.DCForm9.spin_3.get())   #T_end
            self.DDSpinEdit4.set(self.DCForm9.spin_4.get())   #AcqTime
            self.DDSpinEdit9.set(self.DCForm9.spin_9.get())   #t_StartStim
            self.DDSpinEdit10.set(self.DCForm9.spin_10.get()) #t_EndStim
            self.DDSpinEdit1.set(self.DCForm9.spin_64.get())  #I_stim
            self.notebook.select(self.DCForm9.parent.tab1)
            #****************************
            Measurement.RunPatching(self)
            #****************************
            self.Button11.config(fg="black")
        else:
            self.Button11.config(text='Patching')  # Stops current patching and prepares new Patching

    def CleanPlots(self):     # Clean
        # Clear the previous plots
        self.ax[0].clear()
        self.ax[1].clear()
        self.ax_for_G.clear()
        self.canvas.draw()
        self.axPP[0].clear()
        self.axPP[1].clear()
        self.canvasPP.draw()

    # Drawing

    def initialize_drawing(self):
        # Plot on tab1
        self.fig, self.ax = plt.subplots(nrows=2, figsize=(12.5, 6))
        self.ax_for_G = self.ax[1].twinx()
        self.ax[0].set_xlabel("Time, in ms")
        self.ax[1].set_xlabel("Time, in ms")
        self.ax[0].set_ylabel("Voltage, in mV")
        self.ax[1].set_ylabel("Current, in pA")
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.tab1)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.tab1)
        self.toolbar.update()
        self.canvas.get_tk_widget().pack(pady=0, anchor='w')
        globs.dt_draw = float(self.DDSpinEdit27.get())  # ms

        # Plot on tab5 - Passive Properties
        self.figPP, self.axPP = plt.subplots(nrows=2, figsize=(7, 6.5))
        #self.axPP[0].title('Step-current-based estimation of GL and tau_m')
        self.axPP[0].set_xlabel("Current, pA")
        self.axPP[0].set_ylabel("Voltage, mV")
        self.axPP[1].set_xlabel("Time, ms")
        self.axPP[1].set_ylabel("Voltage, mV")
        self.canvasPP = FigureCanvasTkAgg(self.figPP, master=self.tab5)
        self.canvasPP.get_tk_widget().grid(row=0, column=0, padx=50, pady=5, sticky='ew')   # place on tab5


    def DrawArrays(self):
        # Plot the data on the first panel
        self.ax[0].set_xlabel("Time, in ms")
        if self.CheckBox2_Checked.get():
            self.ax[0].plot(globs.Arr_t[:globs.N_Arr], globs.Arr_V[:globs.N_Arr], label="V")
            self.ax[0].set_ylabel("Voltage, in mV")
            self.ax[0].legend()
        # Plot the data on the second panel
        self.ax[1].set_xlabel("Time, in ms")
        if self.CheckBox1_Checked.get():
            self.ax[1].plot(globs.Arr_t[:globs.N_Arr], globs.Arr_Icom[:globs.N_Arr], label="Icom")
            self.ax[1].set_ylabel("Current, in pA")
            self.ax[1].legend()
        if self.CheckBox3_Checked.get():
            self.ax[1].plot(globs.Arr_t[:globs.N_Arr], globs.Arr_Ireal[:globs.N_Arr], label="Ireal")
            self.ax[1].set_ylabel("Current, in pA")
            self.ax[1].legend()
        if self.CheckBox4_Checked.get():
            I_=np.zeros_like(globs.Arr_t)
            G_=np.zeros_like(globs.Arr_t)
            C_=np.zeros_like(globs.Arr_t)
            gNMDA_=np.zeros_like(globs.Arr_t)
            dt_ = globs.Arr_t[2] - globs.Arr_t[1]
            for i in range(globs.N_Arr):
                I_[i], G_[i], C_[i], gNMDA_[i] = Measurement.Define_I_G(globs.Arr_t[i],dt_)
            self.ax[1].plot(globs.Arr_t[:globs.N_Arr], I_[:globs.N_Arr], label="I")
            self.ax[1].legend()
            if globs.CC.g_stim != 0:
                self.ax_for_G.plot(globs.Arr_t[:globs.N_Arr], G_[:globs.N_Arr], label="G")
                self.ax_for_G.set_ylabel('Conductance, nS')
                self.ax_for_G.legend()
        # Display the plot
        self.canvas.draw()

        self.DDSpinEdit1.set(globs.CC.I_stim)  # pA
        self.DDSpinEdit37.set(globs.CC.g_stim)  # nS



if __name__ == "__main__":
    DCForm1 = TDCForm1()
    DCForm1.root.mainloop()
#     form = TDCForm1()
#     form.run()


