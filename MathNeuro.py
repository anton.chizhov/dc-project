import globs

def ReadMathNeuroParameters():
    globs.N_steps = 1
    # Initial conditions
    globs.MN.I = 0
    globs.MN.J = 0

def MN_Define_I(time_,dt_):
    if (globs.CC.IfStimWholeTime==1)or((time_>=globs.t_StartStim)and(time_<globs.t_EndStim)):
        globs.MN.I = globs.MN.I + dt_ / globs.MN.tau * (-globs.MN.J + globs.MN.alpha * globs.V)
        globs.MN.J = globs.MN.J + dt_ / globs.MN.tau * ( globs.MN.I - globs.MN.I0)
    else:
        globs.MN.I = 0
        globs.MN.J = 0
    return globs.MN.I


def RealTime_MathNeuro(time_,dt_):
    globs.Icom = MN_Define_I(time_,dt_)
