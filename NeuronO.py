# {
#   Single neuron (Hodgkin-Huxley) model is coded here.
#   The voltage equation is solved in TNeuron.MembranePotential.
# }

import numpy as np

#{ Parameters: }
class NeuronProperties:
    GL: float
    GNa: float
    GK: float
    VL: float
    C_membr: float

#{ Variables: }
class NeuronVariables:
    V: float
    DVDt: float
    PSC: float
    m: float
    h: float
    n: float

def dexp(x):
    if x < -20:
        return 0  # exp(-20)
    elif x > 20:
        return np.exp(20)
    else:
        return np.exp(x)

#***********************************************************************************************************************
class TNeuron_HH:
#***********************************************************************************************************************
    def __init__(self):
        # Properties:
        self.NP = NeuronProperties()
        self.NV = NeuronVariables()

    def EquateFrom(self, ANrn):
        self.NP = ANrn.NP
        self.NV = ANrn.NV

    def EquateTo(self, ANrn):
        ANrn.NP = self.NP
        ANrn.NV = self.NV


    def Na_current_Chow(self, V, dt, gNa, h):
        tau_h = 0.6 / (1 + dexp(-0.12 * (V + 67)))
        m_inf = 1 / (1 + dexp(-0.08 * (V + 26)))
        h_inf = 1 / (1 + dexp(0.13 * (V + 38)))
        h_exp = 1 - dexp(-dt / tau_h)
        #**************************
        h = h + h_exp * (h_inf - h)
        #**************************
        m = m_inf
        m3 = m * m * m
        I = gNa * m3 * h * (V - 45)
        return I,h


    def K_current_Chow(self, V, dt, gK, n):
        tau_n = 0.5 + 2.0 / (1 + dexp(0.045 * (V - 50)))
        n_inf = 1 / (1 + dexp(-0.045 * (V + 10)))
        n_exp = 1 - dexp(-dt / tau_n)
        # **************************
        n = n + n_exp * (n_inf - n)
        # **************************
        n4 = n * n * n * n
        I = gK * n4 * (V + 80)
        return I, n


    def InitialCanalConductances_Chow(self, V):
        m = 1 / (1 + dexp(-0.08  * (V + 26)))
        h = 1 / (1 + dexp(0.13 * (V + 38)))
        n = 1 / (1 + dexp(-0.045 * (V + 10)))
        return m, h, n

    def ConditionsAtSpike(self, sinceAP):
        pass

    def IfSpikeOccursInThrModel(self):
        pass

    def SetParametersAndInitialConditions(self):
        # Parameters
        self.NP.GNa = 1500  # nS
        self.NP.GK = 2000  # nS
        self.NP.GL = 5  # nS
        self.NP.VL = -70  # mV
        self.NP.C_membr = 100  # pF

        # Initial conditions
        self.NV.V = self.NP.VL
        i = 0
        while i < 100:
            self.NV.m, self.NV.h, self.NV.n = self.InitialCanalConductances_Chow(self.NV.V)
            self.NV.V = self.NP.VL + (-self.NP.GNa * self.NV.m ** 3 * self.NV.h * (self.NV.V - 45) \
                                      -self.NP.GK * self.NV.n ** 4 * (self.NV.V + 80)) / self.NP.GL
            i += 1
        self.NV.PSC = 0

    def MembranePotential(self, uu, ss, Vus_, dt_):
        INa, self.NV.h = self.Na_current_Chow(self.NV.V, dt_, self.NP.GNa, self.NV.h)
        IK,  self.NV.n = self.K_current_Chow(self.NV.V, dt_, self.NP.GK, self.NV.n)
        # ****************************************************************************************************
        self.NV.DVDt = 1 / self.NP.C_membr * (
                    -self.NP.GL * (self.NV.V - self.NP.VL) - INa - IK - ss * (self.NV.V - Vus_) + uu)  # mV/ms
        # ****************************************************************************************************
        self.NV.V = self.NV.V + dt_ * self.NV.DVDt  # mV
        # **********************************************
        self.NV.PSC = -ss * (self.NV.V - self.NP.VL) + uu  # pA
