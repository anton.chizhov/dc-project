# unit RealTime;
# {
#  At each time-step of recordings,
#  RealTimeCalculations_at_OneStep does the following:
#  1) chooses one of the main protocols, for instance, RealTime_CurrentClamp,
#  2) calculates the injected current Icom from known V,
#  3) acquires the DAQ-card and thus sends the signal Icom
#  4) acquires the DAQ-card and thus receives the signals V and Ireal
#  5) compensates the artifacts
#  6) saves the scalar values of V, Icom and Ireal in arrays
#  7) acquire GUI if interuption is allowed in real-clamp
# }

import globs
from Acquisition import *
import CurrentClamp
import MathNeuro
import NoiseO
import Passive


def RealTimeCalculations_at_OneStep(i):
    globs.time = i * globs.AcqTime  # ms
    # *** Protocols ***
    match globs.Protocol:
        case 0:
            pass  # BackGround
        case 1: # Current-Clamp
            CurrentClamp.RealTime_CurrentClamp()
        case 2: # PassiveProperties
            Passive.RealTime_Passive(globs.time)
        case 3: # Tongue
            pass
        case 5: # Dynamic VC
            pass
        case 6: # MathNeuro
            MathNeuro.RealTime_MathNeuro(globs.time,globs.AcqTime)
        case _:
            pass
    # *** SubProtocols ***
    if globs.If_Noise == 1:
        NoiseO.RealTime_Noise()
    if i >= globs.i_end and globs.Protocol != 5:
        globs.Icom = globs.Icom_offset
    # ************************
    DAQ_Write_AO0_AO1()  # (Icom, AO1)
    DAQ_Read_AI0_AI1_AI2()  # (V, Ireal, LFP)
    # ************************

    # # Accelerated version:
    # data = np.zeros((2,), dtype=np.float64)  # array type of two scalar variable is needed for ReadAnalogF64
    # ATask.ReadAnalogF64(1, 10.0, DAQmx_Val_GroupByChannel, data, 3, byref(int32()), None)
    # globs.V = data[0]
    # data3 = (globs.Icom - globs.Icom_offset) / globs.Icom_scale
    # CTask.WriteAnalogScalarF64(1, 10.0, data3, None)

    # *** Online conductance compensation ***
    if globs.If_CondCompensation == 1:
        RealTime_ElectrodeCompensation()
    # ************************
    if not globs.IfNoPlotsInBackground:
        if globs.Protocol == 5:
            pass
        else:
            if i % globs.n_RememberInArr == 0 and globs.i_Arr <= globs.ArrMax:
                globs.i_Arr += 1
                globs.Arr_V[globs.i_Arr]     = globs.V
                globs.Arr_Ireal[globs.i_Arr] = globs.Ireal
                globs.Arr_Icom[globs.i_Arr]  = globs.Icom
                globs.Arr_t[globs.i_Arr]     = globs.time
                # if abs(globs.Arr_V[globs.i_Arr]) > 10 * globs.V_scale:
                #     globs.Arr_V[globs.i_Arr] = 0
                # if abs(globs.Arr_Ireal[globs.i_Arr]) > 10 * globs.Ireal_scale:
                #     globs.Arr_Ireal[globs.i_Arr] = 0

    # Acquire Forms
    # if globs.dt_AcqForms != 0 and globs.time > 0 and globs.time - int(globs.time / globs.dt_AcqForms) * globs.dt_AcqForms < globs.AcqTime / 2:
    #     time.sleep(0)  # Delay to yield control to the system and process GUI events
