# {
#   Global variables are described here.
# }

import numpy as np

global ArrMax,nDrawMax,nSeriesMax,IfNoPlotsInBackground,IfOnline,If_CondCompensation,If_Noise,\
    StartTime,EndTime,Protocol,DeviceNumber,DC_NOfFilesInTheList,Protocol_o,NumberOfDO,i_step,N_steps,\
    i_end,iDrawLimit,N_Arr,n_RememberInArr,Icom_scale,Ireal_scale,V_scale,V_offset,Icom_offset,Ireal_offset,\
    Vmin,Vmax,dVmax,V,Ireal,Icom,Vprev,time,T_end,t_StartStim,t_EndStim,Vus,AcqTime,dt_draw,Epoch,dt_AcqForms,\
    SkipInitialInterval_in_ms,i_Arr,Arr_V,Arr_Ireal,Arr_Icom,Arr_t,Arr_Vcorr,Arr_G1,Arr_Vmod,\
    SubjectiveNote,NameOfLoadedFile,i_step_Shuffled,list,listBack,EC,Noise,CC,BasicColor

BasicColor = "#%02x%02x%02x"
ArrMax = 9000000
nDrawMax = 560000
nSeriesMax = 10000
IfNoPlotsInBackground = False
IfOnline = False
If_CondCompensation = 0
If_Noise = 0
StartTime = 0
EndTime = 0
Protocol = 0
DeviceNumber = 0
DC_NOfFilesInTheList = 0
Protocol_o = 0
NumberOfDO = 0
i_step = 0
N_steps = 0
i = 0
i_end = 0
iDrawLimit = 0
i_Arr = 0
N_Arr = 0
n_RememberInArr = 0
Icom_scale = 0
Ireal_scale = 0
V_scale = 0
V_offset = 0
Icom_offset = 0
Ireal_offset = 0
Vmin = 0
Vmax = 0
dVmax = 0
V = 0
Ireal = 0
Icom = 0
Vprev = 0
time = 0
T_end = 0
t_StartStim = 0
t_EndStim = 0
Vus = 0
AcqTime = 0
dt_draw = 0
Epoch = 0
dt_AcqForms = 0
SkipInitialInterval_in_ms = 0
Arr_V = [0.0] * (ArrMax + 1)
Arr_Ireal = [0.0] * (ArrMax + 1)
Arr_Icom = [0.0] * (ArrMax + 1)
Arr_t = [0.0] * (ArrMax + 1)
Arr_Vcorr = [0.0] * (ArrMax + 1)
Arr_G1 = [0.0] * (ArrMax + 1)
Arr_Vmod = [0.0] * (ArrMax + 1)
SubjectiveNote = ""
NameOfLoadedFile = ""
i_step_Shuffled = [0] * (nSeriesMax + 1)
list = [0] * (nSeriesMax + 1)
listBack = [0] * (nSeriesMax + 1)
TypeOfModelledNeuron = "1"


class EC_ParsVars:
    def __init__(self):
        self.G1 = 0.0
        self.tau_Ifilt = 0.0
        self.tau_Vfilt = 0.0
        self.Ireal_Or_Icom = 0
        self.NstepsOfDelayV = 0
        self.Ireal_prev = 0.0
        self.Ireal_pprev = 0.0
        self.I_filt = 0.0
        self.V_filt = 0.0
        self.V_delayed = [0.0] * 22

EC = EC_ParsVars()


class Noise_ParsVars:
    def __init__(self):
        self.tau_I = 0.0
        self.tau_G = 0.0
        self.sgm_I = 0.0
        self.sgm_G = 0.0
        self.I = 0.0
        self.G = 0.0

Noise = Noise_ParsVars()


class CC_ParsVars:
    def __init__(self):
        self.I_stim = 0.0
        self.g_stim = 0.0
        self.C_stim = 0.0
        self.T_I = 0.0
        self.T_g = 0.0
        self.TypeOfInputIndex = int(0)
        self.IfStimWholeTime = int(0)

CC = CC_ParsVars()


class MN_ParsVars:
    def __init__(self):
        self.I0 = 0.0
        self.tau = 0.0
        self.alpha = 0.0

MN = MN_ParsVars()


class PP_ParsVars:
    def __init__(self):
        self.I_min = 0.0
        self.I_max = 0.0
        self.t1_start = 0.0
        self.t1_end = 0.0
        self.t2_start = 0.0
        self.t2_end = 0.0
        self.G_L = 0.0
        self.tau_m = 0.0
        self.N_steps = 0
        # Variables:
        self.Vstep = 0.0
        self.Vbg = 0.0
        self.Icom_step = 0.0
        self.axPP_x = []
        self.axPP_y = []


PP = PP_ParsVars()


#{ Series ===================================================================== }
class TResultsOfTrial:
    PP                                                 =PP_ParsVars()
    EC                                                 =EC_ParsVars()
    CC                                                 =CC_ParsVars()

ResultsOfSeries = [TResultsOfTrial()] * (200)



def IfTrue(x):
    if x:
        return 1
    else:
        return 0
