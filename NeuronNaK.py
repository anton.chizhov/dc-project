# {
#   Single neuron (Hodgkin-Huxley) model is coded here.
#   The voltage equation is solved in TNeuron.MembranePotential.
# }

import numpy as np

#{ Parameters: }
class NeuronProperties:
    GL: float
    GNa: float
    GK: float
    VL: float
    C_membr: float

#{ Variables: }
class NeuronVariables:
    V: float
    DVDt: float
    PSC: float
    m: float
    h: float
    n: float

def dexp(x):
    if x < -20:
        return 0  # exp(-20)
    elif x > 20:
        return np.exp(20)
    else:
        return np.exp(x)

#***********************************************************************************************************************
class TNeuron_NaK:
#***********************************************************************************************************************
    def __init__(self):
        # Properties:
        self.NP = NeuronProperties()
        self.NV = NeuronVariables()

    def EquateFrom(self, ANrn):
        self.NP = ANrn.NP
        self.NV = ANrn.NV

    def EquateTo(self, ANrn):
        ANrn.NP = self.NP
        ANrn.NV = self.NV


    def Na_current(self, V, gNa):
        m_inf = 1 / (1 + dexp((-20 - V) / 15))
        I = gNa * m_inf * (V - 60)
        return I

    def K_current(self, V, dt, gK, n):
        tau_n = 1
        n_inf = 1 / (1 + dexp((-25 - V) / 5))
        n_exp = 1 - dexp(-dt / tau_n)
        # **************************
        n = n + n_exp * (n_inf - n)
        # **************************
        I = gK * n * (V + 90)
        return I, n

    def SetParametersAndInitialConditions(self):
        # Parameters
        self.NP.GNa = 20  # nS
        self.NP.GK = 10  # nS
        self.NP.GL = 8  # nS
        self.NP.VL = -80  # mV
        self.NP.C_membr = 1  # pF

        # Initial conditions
        self.NV.V = self.NP.VL
        i = 0
        while i < 100:
            self.NV.n = 1 / (1 + dexp((-25 - self.NV.V) / 5))
            INa_ = self.Na_current(self.NV.V, self.NP.GNa)
            self.NV.V = self.NP.VL + (-INa_ - self.NP.GK * self.NV.n * (self.NV.V + 90)) / self.NP.GL
            i += 1
        self.NV.PSC = 0

    def MembranePotential(self, uu, ss, Vus_, dt_):
        INa = self.Na_current(self.NV.V, self.NP.GNa)
        IK,  self.NV.n = self.K_current(self.NV.V, dt_, self.NP.GK, self.NV.n)
        IL = self.NP.GL * (self.NV.V - self.NP.VL)
        # ****************************************************************************************************
        self.NV.DVDt = 1 / self.NP.C_membr * ( - IL - INa - IK - ss * (self.NV.V - Vus_) + uu )  # mV/ms
        # ****************************************************************************************************
        self.NV.V = self.NV.V + dt_ * self.NV.DVDt  # mV
        # **********************************************
        self.NV.PSC = -ss * (self.NV.V - self.NP.VL) + uu  # pA
