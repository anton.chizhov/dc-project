import tkinter as tk
from tkinter import ttk, Frame
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import Spinbox, LabelFrame, Label
import Measurement
#import globs


class SpinEdit(LabelFrame):
    def __init__(self, parent, label, min_value, max_value, default_value, increment=1):
        super().__init__(parent, text=label)

        # create the spinbox
        self.spin_box = Spinbox(self, from_=min_value, to=max_value, increment=increment, width=10)
        self.spin_box.pack(side='right')

        # create the label
        self.label = Label(self, text=label + ':')
        self.label.pack(side='left')

        # set the default value
        self.set_value(default_value)

    def set_value(self, value):
        self.spin_box.delete(0, 'end')
        self.spin_box.insert(0, value)

    def get_value(self):
        return float(self.spin_box.get())


class TDCForm9(tk.Toplevel):
    """
    GUI, Form9 for patching procedure (the button 'Patching' on the main form).
    """

    def __init__(self,parent):
        #super().__init__()
        self.parent = parent
        self.root = tk.Tk()
        self.root.configure(bg='#F0C8A4')
        self.root.title('DCForm9')

        # create a custom style with a light blue background color
        style = ttk.Style()
        style.configure('Custom.TFrame', background='#E6F2F2')

        self.notebook = ttk.Notebook(self.root)
        self.notebook.pack(fill='both', expand=True)

        self.tab1 = ttk.Frame(self.notebook, style='Custom.TFrame')
        self.notebook.add(self.tab1, text='Tab 1')

        # self.fig = plt.figure(figsize=(4,3), dpi=100)
        # self.canvas = FigureCanvasTkAgg(self.fig, master=self.tab1)
        # self.canvas.get_tk_widget().grid(row=4, column=0, padx=10, pady=10)

        # GUI Widgets

        # self.Button11 = ttk.Button(self.tab1, text="Patching", command=self.Button11_click)
        # self.Button11.grid(row=0, column=0, padx=10, pady=10)
        self.group_box2 = ttk.LabelFrame(self.tab1, text='Time')
        self.group_box2.grid(row=1, column=0, padx=5, pady=5, sticky='ew')
        #self.group_box2.configure(style="Custom.TLabelframe", background=globs.BasicColor)
        self.group_box3 = ttk.LabelFrame(self.tab1, text='Other parameters')
        self.group_box3.grid(row=2, column=0, padx=5, pady=5, sticky='ew')

        ttk.Label(self.group_box2, text='T_end').grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.spin_3 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=50, width=10, command=self.spin_3_click)
        self.spin_3.insert(0,"100")
        self.spin_3.grid(row=0, column=1, padx=5, pady=5)

        ttk.Label(self.group_box2, text='AcqTime, ms').grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.spin_4 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=0.05, width=10, command=self.spin_4_click)
        self.spin_4.insert(0,"0.1")
        self.spin_4.grid(row=1, column=1, padx=5, pady=5)

        ttk.Label(self.group_box2, text='t_StartStim, ms').grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.spin_9 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=10, width=10, command=self.spin_9_click)
        self.spin_9.insert(0,"10")
        self.spin_9.grid(row=2, column=1, padx=5, pady=5)

        ttk.Label(self.group_box2, text='t_EndStim, ms').grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.spin_10 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=10, width=10, command=self.spin_10_click)
        self.spin_10.insert(0,"60")
        self.spin_10.grid(row=3, column=1, padx=5, pady=5)

        ttk.Label(self.group_box2, text='Number of iterations').grid(row=4, column=0, padx=5, pady=5, sticky='w')
        self.spin_1 = ttk.Spinbox(self.group_box2, from_=0, to=1000, increment=1000, width=10)
        self.spin_1.insert(0,"2000")
        self.spin_1.grid(row=4, column=1, padx=5, pady=5)


        ttk.Label(self.group_box3, text='I_stim, pA').grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.spin_64 = ttk.Spinbox(self.group_box3, from_=-1000, to=1000, increment=10, width=10, command=self.spin_64_click)
        self.spin_64.insert(0,"10")
        self.spin_64.grid(row=0, column=1, padx=5, pady=5)


        ttk.Label(self.group_box3, text='Vh0, mV').grid(row=1, column=0, padx=5, pady=5, sticky='w')
        self.spin_74 = ttk.Spinbox(self.group_box3, from_=-150, to=100, increment=1, width=10)
        self.spin_74.insert(0,"-50")
        self.spin_74.grid(row=1, column=1, padx=5, pady=5)

        ttk.Label(self.group_box3, text='Vh1, mV').grid(row=2, column=0, padx=5, pady=5, sticky='w')
        self.spin_85 = ttk.Spinbox(self.group_box3, from_=-150, to=100, increment=1, width=10)
        self.spin_85.insert(0,"-60")
        self.spin_85.grid(row=2, column=1, padx=5, pady=5)

        ttk.Label(self.group_box3, text='GL, nS').grid(row=3, column=0, padx=5, pady=5, sticky='w')
        self.spin_87 = ttk.Spinbox(self.group_box3, from_=0, to=1000, increment=1, width=10)
        self.spin_87.insert(0,"0")
        self.spin_87.grid(row=3, column=1, padx=5, pady=5)

        ttk.Label(self.group_box3, text='Rin, MOhm').grid(row=4, column=0, padx=5, pady=5, sticky='w')
        self.spin_2 = ttk.Spinbox(self.group_box3, from_=0, to=1000, increment=0.1, width=10)
        self.spin_2.insert(0,"0")
        self.spin_2.grid(row=4, column=1, padx=5, pady=5)

        ttk.Label(self.group_box3, text='VL, mV').grid(row=5, column=0, padx=5, pady=5, sticky='w')
        self.spin_73 = ttk.Spinbox(self.group_box3, from_=-150, to=100, increment=0.1, width=10)
        self.spin_73.insert(0,"-65")
        self.spin_73.grid(row=5, column=1, padx=5, pady=5)

        ttk.Label(self.group_box3, text='Ga, nS').grid(row=6, column=0, padx=5, pady=5, sticky='w')
        self.spin_100 = ttk.Spinbox(self.group_box3, from_=0, to=1000, increment=0.1, width=10)
        self.spin_100.insert(0,"0")
        self.spin_100.grid(row=6, column=1, padx=5, pady=5)

        self.root.withdraw()

    def spin_3_click(self):
        self.parent.DDSpinEdit3.set(self.spin_3.get())
        Measurement.SetParametersFromForms(self.parent)

    def spin_4_click(self):
        self.parent.DDSpinEdit4.set(self.spin_4.get())
        Measurement.SetParametersFromForms(self.parent)

    def spin_9_click(self):
        self.parent.DDSpinEdit9.set(self.spin_9.get())
        Measurement.SetParametersFromForms(self.parent)

    def spin_10_click(self):
        self.parent.DDSpinEdit10.set(self.spin_10.get())
        Measurement.SetParametersFromForms(self.parent)

    def spin_64_click(self):
        self.parent.DDSpinEdit1.set(self.spin_64.get())
        Measurement.SetParametersFromForms(self.parent)

    def Button11_click(self):

        #***********************************
        Measurement.RunPatching(self.parent)
        #***********************************

    def on_button_click222(self):
        x_max = 2*np.pi
        x = np.linspace(0, x_max, 100)
        y = np.sin(x)
        self.fig.clf()
        ax = self.fig.add_subplot(111)
        ax.plot(x, y)
        ax.set_xlim([0, x_max])
        self.canvas.draw()

    def Form9Hide(self):
        self.parent.DDSpinEdit3.set(self.x3)   #T_end
        self.parent.DDSpinEdit4.set(self.x4)   #AcqTime
        self.parent.DDSpinEdit9.set(self.x9)   #t_StartStim
        self.parent.DDSpinEdit10.set(self.x10) #t_EndStim
        self.parent.DDSpinEdit1.set(self.x64)  #I_stim
        self.root.withdraw()

    def run(self):
        self.root.mainloop()



#DCForm9 :TDCForm9

#DCForm9 = TDCForm9()

# if __name__ == "__main__":
#     form = TDCForm9()
#     form.run()


