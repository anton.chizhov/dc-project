# unit NoiseO;
# {
#   Noise can be added to the current injected into neuron.
#   It is current or conductance, white or color Gaussian noise.
# }

import random
from math import sqrt
import globs


def ReadNoiseParameters(DCForm1):
    # Parameters
    globs.Noise.tau_I = float(DCForm1.DDSpinEdit68.get())  # ms
    globs.Noise.tau_G = float(DCForm1.DDSpinEdit69.get())  # ms
    globs.Noise.sgm_I = float(DCForm1.DDSpinEdit70.get())  # pA
    globs.Noise.sgm_G = float(DCForm1.DDSpinEdit71.get())  # nS
    # Initial conditions
    globs.Noise.I = 0
    globs.Noise.G = 0


def RealTime_Noise():
    # Ornstein-Uhlenbeck noise
    globs.Noise.I = globs.Noise.I - globs.AcqTime / globs.Noise.tau_I *globs. Noise.I \
                    + globs.Noise.sgm_I * sqrt(2 * globs.AcqTime / globs.Noise.tau_I) * random.gauss(0, 1)
    globs.Noise.G = globs.Noise.G - globs.AcqTime / globs.Noise.tau_G * globs.Noise.G \
                    + globs.Noise.sgm_G * sqrt(2 * globs.AcqTime / globs.Noise.tau_G) * random.gauss(0, 1)

    # *********
    globs.Icom = globs.Icom + globs.Noise.I - globs.Noise.G * (globs.V - globs.Vus)
    # *********

