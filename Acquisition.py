# The file contains commands sent to the acquisition card.
# Connections are through AI0/GND, AI1/GND and AO0/GND.
# The commands call the driver's commands, provided by National Instruments.
# It uses 8 commands of nidaqmx-driver:
# Task, CreateAIVoltageChan, CreateAOVoltageChan, CfgSampClkTiming, StartTask,
# ReadAnalogF64, WriteAnalogScalarF64 and StopTask.
# Three signals are sent and received: V, Icom and Ireal.

from PyDAQmx.DAQmxFunctions import *
from PyDAQmx import *
import numpy as np
from ArtMode import Init_ArtificialNeuron, Acquire_ArtificialNeuron
import globs


def DAQ_Initialize_AI0_AI1_AI2_AO0(rate, DeviceNumber, TypeOfModelledNeuron):
    global ATask, CTask, DTask
    if DeviceNumber == 0:
        Init_ArtificialNeuron(TypeOfModelledNeuron)
        k=1
    else:
        Dev = "Dev1/"
        if   DeviceNumber == 2:
            Dev = 'Dev2/'
        elif DeviceNumber == 3:
            Dev = 'Dev2/'
        elif DeviceNumber == 4:
            Dev = 'Dev4/'
        elif DeviceNumber == 5:
            Dev = 'Dev5/'
        chanAI = Dev+'ai0:2'
        chanAO = Dev+'ao0'
        chanPO = Dev+'port0/line0:1'

        try :
            ATask = Task('')
            CTask = Task('')
            DTask = Task('')

            ATask.CreateAIVoltageChan(chanAI, "", DAQmx_Val_RSE, -10.0, 10.0, DAQmx_Val_Volts, None)
            CTask.CreateAOVoltageChan(chanAO, "",                -10.0, 10.0, DAQmx_Val_Volts, None)
            DTask.CreateDOChan(chanPO, "", DAQmx_Val_ChanForAllLines)

            ATask.CfgSampClkTiming("", rate, DAQmx_Val_Falling, DAQmx_Val_HWTimedSinglePoint, 2)
            CTask.CfgSampClkTiming("", rate, DAQmx_Val_Rising,  DAQmx_Val_HWTimedSinglePoint, 1)

            ATask.StartTask()
            CTask.StartTask()
            DTask.StartTask()
            k=1

        except DAQError as err:
            globs.DeviceNumber = 0
            Init_ArtificialNeuron(TypeOfModelledNeuron)
            k = -1
            print('Error in DAQ_Initialize_AI0_AI1_AI2_AO0:', err)
    return k



def DAQ_Stop_AI0_AI1_AI2_AO0_AO1_D0_D1():
    if globs.DeviceNumber != 0:
        try :
            ATask.StopTask()
            CTask.StopTask()
            DTask.StopTask()
        except DAQError as err:
            print('Error in DAQ_Stop_AI0_AI1_AI2_AO0_AO1_D0_D1:', err)

def DAQ_Write_AO0_AO1():
    # Sends Icom
    if globs.DeviceNumber == 0:
        Acquire_ArtificialNeuron()
    else:
        # *** Injected Current ***
        data3 = (globs.Icom - globs.Icom_offset)/globs.Icom_scale
        # Check physiological limits
        if (globs.V>globs.Vmax)or(globs.V<globs.Vmin)or(((globs.V-globs.Vprev)**2>globs.dVmax**2)and(globs.Vprev!=0))or(data3**2>100):
            data3=0
        CTask.WriteAnalogScalarF64(1, 10.0, data3, None)

def DAQ_Write_D0_D1_D2_D3():
    if globs.DeviceNumber != 0:
        data3[0]=globs.Dig.DO0
        data3[1]=globs.Dig.DO1
        DTask.WriteDigitalLines(1, 1, 10.0, PyDAQmx.DAQmx_Val_GroupByChannel, data3, None, None)

def DAQ_Read_AI0_AI1_AI2():
    # Receives V and Ireal
    if globs.DeviceNumber != 0:
        # *** Register voltage and real current ***
        globs.Vprev=globs.V
        data1 = np.zeros((2,), dtype=np.float64)  # array type of two scalar variable is needed for ReadAnalogF64
        ATask.ReadAnalogF64(1, 10.0, DAQmx_Val_GroupByChannel, data1, 3, byref(int32()), None)
        # Check acquisition limits
        if (data1[0]>10) or (data1[0]<-10):
            data1[0]=0
        if (data1[1]>10) or (data1[1]<-10):
            data1[1]=0
        globs.V     = data1[0]*globs.V_scale     + globs.V_offset
        globs.Ireal = data1[1]*globs.Ireal_scale + globs.Ireal_offset


def DAQ_SingleCommand_AO0_AO1_AI0_AI1_AI2(if_VC,  rate, DeviceNumber, TypeOfModelledNeuron):
    DAQ_Initialize_AI0_AI1_AI2_AO0(rate, DeviceNumber, TypeOfModelledNeuron)
    globs.Icom=0
    DAQ_Write_AO0_AO1()
    DAQ_Read_AI0_AI1_AI2()
    #pause(40)
    DAQ_Stop_AI0_AI1_AI2_AO0_AO1_D0_D1(DeviceNumber)
