import globs

def ReadPassiveParameters():
    globs.N_steps = globs.PP.N_steps
    globs.i_end = int(1000 / globs.AcqTime)

def PP_Define_I_G(time_):
    I = 0
    G = globs.CC.g_stim
    C = globs.CC.C_stim
    if time_ >= globs.t_StartStim and time_ < globs.t_EndStim:
        globs.CC.I_stim  = globs.PP.I_min + (globs.PP.I_max - globs.PP.I_min) / (globs.PP.N_steps - 1) * (globs.i_step - 1)
        I = globs.CC.I_stim
    return I, G, C

def RealTime_Passive(time_):
    I_, G_, C_ = PP_Define_I_G(time_)
    globs.Icom = I_ - G_ * (globs.V - globs.Vus) - C_ * (globs.V - globs.Vprev) / globs.AcqTime

