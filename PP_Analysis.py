import math
import matplotlib.pyplot as plt
from tkinter import Tk
import globs
from Measurement import RunOnce, RunSeries
import Passive
import numpy as np


# Initialize Tkinter
root = Tk()
root.withdraw()

def DrawLineWithSlope_G(DCForm1):
    globs.PP.G_L = float(DCForm1.DDSpinEdit18.get())  # nS
    #fig, ax = plt.subplots(nrows=2, figsize=(16, 12))
    DCForm1.axPP[0].set_xlabel("Current, pA")
    DCForm1.axPP[0].set_ylabel("Voltage, mV")
    DCForm1.axPP[1].set_xlabel("Time, ms")
    DCForm1.axPP[1].set_ylabel("Voltage, mV")
    # Line
    if globs.PP.G_L > 0:
        x = [-100, 100]
        y = [-100 / globs.PP.G_L, 100 / globs.PP.G_L]
        DCForm1.axPP[0].plot(x, y, label="linear approximation")
    # Dots
    DCForm1.axPP[0].plot(globs.PP.axPP_x, globs.PP.axPP_y, marker='.', linestyle='', label="dots from step measurements")
    DCForm1.axPP[0].legend()
    DCForm1.canvasPP.draw()


def Estimate_tau_m(DCForm1):
    # Estimate using analysis of the last trace
    if globs.AcqTime == 0 or globs.PP.tau_m == 0:
        return
    dV_ = abs(globs.PP.Vstep - globs.PP.Vbg)
    S_ = 0
    ip = 0
    dt_ = globs.AcqTime * globs.n_RememberInArr
    istart = int(globs.t_StartStim / dt_)
    for i in range(istart - int(20 / dt_), istart + int(10 * globs.PP.tau_m / dt_)):
        V_ = globs.Arr_V[i] - globs.PP.Vbg
        if (i * dt_ > globs.t_StartStim) and (abs(V_) > dV_/3) and (abs(V_) < dV_*2/3):
            ip += 1
            #*** membrane time constant ********************************************************************
            tau_i_ = -(i * dt_ - globs.t_StartStim) / math.log( 1 - abs(globs.Arr_V[i] - globs.PP.Vbg)/dV_ )
            #***********************************************************************************************
            S_ = (S_ * (ip - 1) + tau_i_) / ip
    globs.PP.tau_m = S_
    DCForm1.DDSpinEdit54.set(globs.PP.tau_m)  # ms
    DCForm1.DDSpinEdit130.set(globs.PP.tau_m * globs.PP.G_L)  # pF


def DrawExponentialFit_taum(DCForm1):
    globs.PP.tau_m = float(DCForm1.DDSpinEdit54.get())  # ms
    DCForm1.DDSpinEdit130.set(globs.PP.tau_m * globs.PP.G_L)  # pF
    if globs.AcqTime == 0 or globs.PP.tau_m == 0:
        return
    dt_ = globs.AcqTime * globs.n_RememberInArr
    istart = int(globs.t_StartStim / dt_)
    #plt.figure()
    x_values = []
    y1_values = []
    y2_values = []
    for i in range(istart - int(20 / dt_), istart + int(min(10 * globs.PP.tau_m, 500) / dt_)):
        t_ = i * dt_
        if t_ <= globs.t_StartStim:
            Vexponent = globs.PP.Vbg
        else:
            Vexponent = globs.PP.Vbg + (globs.PP.Vstep - globs.PP.Vbg) * (1 - math.exp(-(t_ - globs.t_StartStim) / globs.PP.tau_m))
        if globs.dt_draw == 0 or (t_ - int(t_ / globs.dt_draw) * globs.dt_draw < dt_ / 2):
            x_values.append(t_)
            y1_values.append(globs.Arr_V[i])
            y2_values.append(Vexponent)
    # Plotting
    DCForm1.axPP[1].plot(x_values, y1_values, 'b.', label="V(t)")
    DCForm1.axPP[1].plot(x_values, y2_values, 'r.', label="exponential fit")
    DCForm1.axPP[1].legend()
    DCForm1.canvasPP.draw()


def AnalysePassive(DCForm1):
    k1 = 0
    k2 = 0
    i = -1
    while i < globs.N_Arr:
        i += 1
        V = globs.Arr_V[i]
        #Ireal = globs.Arr_Ireal[i]
        #Icom = globs.Arr_Icom[i]
        time = globs.Arr_t[i]
        if time >= globs.PP.t1_start and time < globs.PP.t1_end:
            k1 += 1
            globs.PP.Vbg = globs.PP.Vbg * (k1 - 1) / k1 + V / k1
        if time >= globs.PP.t2_start and time < globs.PP.t2_end:
            k2 += 1
            globs.PP.Vstep = globs.PP.Vstep * (k2 - 1) / k2 + V / k2
            globs.PP.Icom_step, G_, C_ = Passive.PP_Define_I_G(time)
    # Saving dots for plotting
    globs.PP.axPP_x.append(globs.PP.Icom_step)
    globs.PP.axPP_y.append(globs.PP.Vstep - globs.PP.Vbg)
    # Plotting line and dots
    DrawLineWithSlope_G(DCForm1)
    # Plotting tau_m
    DrawExponentialFit_taum(DCForm1)
    DCForm1.canvasPP.draw()


def Estimate_GL(DCForm1):
    # Mean over series
    N_ = 0
    GL_ = 0.0
    for i in range(1, globs.N_steps + 1):
        if abs(globs.ResultsOfSeries[i].PP.Icom_step) > 0.1:  # pA
            GL_ += globs.ResultsOfSeries[i].PP.Icom_step / (globs.ResultsOfSeries[i].PP.Vstep - globs.ResultsOfSeries[i].PP.Vbg)
            N_ += 1
    if GL_ > 0.1:
        DCForm1.DDSpinEdit18.set(GL_ / N_)  # G_L, in nS


def AutomaticEstimationOf_GL(DCForm1):
    #DCForm1.ComboBox2.current(0)   # DeviceNumber      !!!
    I_stim_mem = globs.CC.I_stim
    globs.Protocol = 2  # Passive
    DCForm1.RadioGroup2_value.set(globs.Protocol)

    DCForm1.Button20.configure(state="disabled")
    DCForm1.notebook.select(DCForm1.tab5)  # activate tab "PassiveProperties"

    #DCForm1.DDSpinEdit5.set(-10)  # pA (PP.I_min)
    DCForm1.DDSpinEdit3.set(1000)  # ms (T_end)

    i = 0
    while abs(globs.PP.Vstep - globs.PP.Vbg) < 5 and i < 10:  # mV
        i += 1
        DCForm1.notebook.select(DCForm1.tab1)  # activate tab "Records"
        #*************************
        RunOnce(DCForm1)
        AnalysePassive(DCForm1)
        #*************************
        DCForm1.notebook.select(DCForm1.tab5)  # activate tab "PassiveProperties"

        if abs(globs.PP.Vstep - globs.PP.Vbg) < 5:  # mV
            DCForm1.DDSpinEdit5.set(float(DCForm1.DDSpinEdit5.get()) - 10)  # pA (I_min:=I_min-10)

    DCForm1.DDSpinEdit5.set(float(DCForm1.DDSpinEdit5.get()) * 1.2)  # pA (PP.I_min)
    DCForm1.DDSpinEdit6.set(5)  # pA (PP.I_max)
    DCForm1.DDSpinEdit7.set(5)  # N_steps
    globs.N_steps = int(DCForm1.DDSpinEdit7.get())
    # DCForm1.PointSeries1.Clear()
    # DCForm1.TabSheet6.Show()
    DCForm1.CleanPlots()
    globs.PP.axPP_x=[]
    globs.PP.axPP_y=[]

    DCForm1.notebook.select(DCForm1.tab1)  # activate tab "Records"
    IfForward_ = (DCForm1.DDSpinEdit6.get()>DCForm1.DDSpinEdit5.get())
    #****************************
    RunSeries(DCForm1,IfForward_)
    #****************************
    DCForm1.axPP[0].clear()
    DCForm1.axPP[1].clear()
    Estimate_GL(DCForm1)
    Estimate_tau_m(DCForm1)
    DrawLineWithSlope_G(DCForm1)
    DrawExponentialFit_taum(DCForm1)
    globs.CC.I_stim = I_stim_mem
    DCForm1.DDSpinEdit1.set(float(globs.CC.I_stim))

    DCForm1.notebook.select(DCForm1.tab5)  # activate tab "PassiveProperties"
    DCForm1.Button20.configure(state="normal")
    DCForm1.root.update()


