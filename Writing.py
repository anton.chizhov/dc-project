#unit Writing;
# {
#   Subroutines for writing the recorded data.
#   One file of data and one of parameters are written for each trace.
#   The working directory is set in CreateDirectoryForNewCell and
#   CreateWorkingDirectory that are run from the main form of GUI.
#   The directory is named with the date of
#   recordings and the number of the patched cell.
#   The trace is written with WriteTraceInFile, where the name of the file
#   is set, containing the information about the time of the recordings,
#   the error time of acquisition, and the type of protocol.
#   ParamsToDCForm copies the parameters from previously recorded file of
#   parameters to GUI.
#   DCReadListOfFiles reads list of files in the working  directory.
#   DCReadPastTrace read the last trace or the one that was clicked by mouse.
#   ExportDataInClampfitFormat translates data in the format readable by Clampfit.
# }

import datetime
import os
import io
import struct
import globs
import ReadWrite_pars



def WriteTraceInFile(DCForm1):
    if not DCForm1.CheckBox9_Checked:
        return

    # File name
    root_dir = os.getcwd()
    now = datetime.datetime.now()
    hour = now.hour
    minute = now.minute
    second = now.second
    msecond = now.microsecond // 1000
    fname = f"{hour}_{minute}_{second}"
    fname_lfp = f"{fname}.lfp"
    fname_params = f"{fname}.par"
    fname = f"{fname}_trace{str(globs.i_step)}"

    # Errors
    fname = f"{fname}_Err{str(int(globs.EndTime - globs.StartTime - globs.T_end))}"

    # Protocol name in extension
    if globs.Protocol == 0:
        fname = f"{fname}.bg"
    elif globs.Protocol == 1:
        fname = f"{fname}.cc"
    elif globs.Protocol == 2:
        fname = f"{fname}.pp"
    elif globs.Protocol == 3:
        fname = f"{fname}.us"
    elif globs.Protocol == 4:
        fname = f"{fname}.smc"
    elif globs.Protocol == 5:
        fname = f"{fname}.VC"
    elif globs.Protocol == 6:
        fname = f"{fname}.mn"

    # Binary file
    fname_b = f"{fname}_b"

    # Binary or text
    if_t = False
    if_b = False

    i_Combo = DCForm1.ComboBox8.current()
    if i_Combo == 0:
        if_t = True
        if_b = True
    elif i_Combo == 1:
        if_t = True
    elif i_Combo == 2:
        if_b = True

    if if_t:  # TEXT
        bbb = open(fname, "w")

    if if_b:  # BINARY
        ccc = open(fname_b, "wb")
        ccc.write(struct.pack('d', globs.AcqTime))
        t1 = 'Signals: V, Icom, Ireal'
        t1 = t1.ljust(256, '\x00')
        ccc.write(t1.encode('utf-8'))

        # Scaling: V = V16/32768*10*V_scale + V_offset
        sc_ = [10 / 32768 * globs.V_scale, 10 / 32768 * globs.Icom_scale, 10 / 32768 * globs.Ireal_scale]
        off_ = [globs.V_offset, globs.Icom_offset, globs.Ireal_offset]
        for val in sc_:
            ccc.write(struct.pack('d', val))
        for val in off_:
            ccc.write(struct.pack('d', val))

    # Writing signals
    for i in range(globs.N_Arr):
        V = globs.Arr_V[i]
        Ireal = globs.Arr_Ireal[i]
        Icom = globs.Arr_Icom[i]
        time = globs.Arr_t[i]

        if if_t:  # TEXT
            bbb.write(f"{time:10.3f} {V:10.3f} {Icom:10.3f} {Ireal:12.3f}\n")

        if if_b:  # BINARY
            V16 = int((V - off_[0]) / sc_[0])
            Icom16 = int((Icom - off_[1]) / sc_[1])
            Ireal16 = int((Ireal - off_[2]) / sc_[2])
            ccc.write(struct.pack('d', V16))
            ccc.write(struct.pack('d', Icom16))
            ccc.write(struct.pack('d', Ireal16))

    if if_t:
        bbb.close()

    if if_b:
        ccc.close()

    # Writing parameters
    ReadWrite_pars.write_parameters_to(fname_params)

    # List of files
    fn_list = 'ListOfFiles'

    with open(fn_list, 'a+') as ddd:
        ddd.write(f"{fname}\n")
        ddd.write(f"{fname_params}\n")

    DCForm1.Folders.list_files()


def ParamsToDCForm(DCForm1):
    DCForm1.DDSpinEdit3.set(globs.T_end)  # ms
    DCForm1.DDSpinEdit4.set(globs.AcqTime)  # ms
    DCForm1.DDSpinEdit5.set(globs.PP.I_min)       # pA
    DCForm1.DDSpinEdit6.set(globs.PP.I_max)       # pA
    DCForm1.DDSpinEdit7.set(globs.PP.N_steps)
    DCForm1.DDSpinEdit14.set(globs.PP.t1_start)
    DCForm1.DDSpinEdit15.set(globs.PP.t1_end)
    DCForm1.DDSpinEdit16.set(globs.PP.t2_start)
    DCForm1.DDSpinEdit17.set(globs.PP.t2_end)
    DCForm1.DDSpinEdit18.set(globs.PP.G_L)
    DCForm1.DDSpinEdit54.set(globs.PP.tau_m)
    DCForm1.DDSpinEdit9.set(globs.t_StartStim)  # ms
    DCForm1.DDSpinEdit10.set(globs.t_EndStim)  # ms
    if globs.Protocol != 5:
        DCForm1.DDSpinEdit25.set(globs.V_scale)  # mV/V
        DCForm1.DDSpinEdit26.set(globs.Icom_scale)  # pA/V
        DCForm1.DDSpinEdit31.set(globs.Ireal_scale)  # pA/V
        DCForm1.DDSpinEdit39.set(globs.V_offset)  # mV
        DCForm1.DDSpinEdit38.set(globs.Icom_offset)  # pA
        DCForm1.DDSpinEdit40.set(globs.Ireal_offset)  # pA
    DCForm1.DDSpinEdit27.set(globs.dt_draw)  # ms
    DCForm1.DDSpinEdit28.set(globs.Epoch / 1e3)  # s
    DCForm1.DDSpinEdit1.set(globs.CC.I_stim)  # pA
    DCForm1.DDSpinEdit37.set(globs.CC.g_stim)  # nS
    DCForm1.DDSpinEdit2.set(globs.CC.T_I)  # ms
    DCForm1.DDSpinEdit30.set(globs.Vus)
    DCForm1.DDSpinEdit52.set(globs.CC.T_g)  # ms
    DCForm1.ComboBox1.current(globs.CC.TypeOfInputIndex)
    DCForm1.DDSpinEdit53.set(globs.CC.C_stim)
    DCForm1.DDSpinEdit56.set(globs.EC.G1)
    DCForm1.ComboBox2.current(globs.DeviceNumber)
    DCForm1.CheckBox5_Checked.set(globs.If_CondCompensation == 1)
    DCForm1.DDSpinEdit57.set(globs.EC.tau_Vfilt)
    DCForm1.DDSpinEdit58.set(globs.EC.tau_Ifilt)
    DCForm1.Sketchview1_Checked.set(float(DCForm1.DDSpinEdit27.get()) != 0)  # dt_draw
    DCForm1.DDSpinEdit80.set(globs.Vmin)  # mV
    DCForm1.DDSpinEdit81.set(globs.Vmax)  # mV
    DCForm1.DDSpinEdit82.set(globs.dVmax)  # mV
    DCForm1.DDSpinEdit142.set(globs.n_RememberInArr)
    DCForm1.DDSpinEdit165.set(globs.MN.I0)  # pA
    DCForm1.DDSpinEdit166.set(globs.MN.tau)  # ms
    DCForm1.DDSpinEdit167.set(globs.MN.alpha)  # nS
    # ---------
    DCForm1.CheckBox18_Checked.set(globs.CC.IfStimWholeTime == 1)
    DCForm1.SubjectiveNote.insert("end", globs.SubjectiveNote)
    # Settings that must be at the end. They were replaced here from above.
    DCForm1.RadioGroup2_value.set(globs.Protocol)
    DCForm1.root.configure(bg=globs.BasicColor)
    DCForm1.Panel1.configure(bg=globs.BasicColor)
    #DCForm1.frame.configure(bg=globs.BasicColor)
    DCForm1.style.configure('My.TFrame', background=globs.BasicColor)
    DCForm1.style.configure("My.TLabelframe", background=globs.BasicColor)
    DCForm1.style.configure("My.TLabel", background=globs.BasicColor)
    DCForm1.ComboBox44.current(globs.TypeOfModelledNeuron)


def dc_read_list_of_files(DCForm1):
    fn_list = 'ListOfFiles'
    DCForm1.ListBox1.Clear()

    if os.path.exists(fn_list):
        with open(fn_list, 'r') as ddd:
            DC_NOfFilesInTheList = 0
            for line in ddd:
                fname = line.strip()
                fname_params = ddd.readline().strip()
                DCForm1.ListBox1.Items.Add(fname)
                DC_NOfFilesInTheList += 1

    # Close the file if it was opened
    if 'ddd' in locals():
        ddd.close()

    # Update the GUI
    Application.ProcessMessages()

    # Read Past Parameters
    if DCForm1.CheckBox12.Checked:
        read_parameters_of(fname_params)
        params_to_dc_form()


def extract_trace_number(fname):
    i1 = fname.find('trace') + 5
    i2 = fname.find('_Err')
    if i1 > 0:
        s = fname[i1:i2]
        globs.i_step = int(s)


def DCReadPastTrace(fname,DCForm1):
    # Read parameters
    fname_params = fname[:fname.find('_trace')] + '.par'
    # Read parameters from file
    if DCForm1.CheckBox12_Checked:
        ReadWrite_pars.ReadParametersOf(fname_params)
        ParamsToDCForm(DCForm1)
    globs.SkipInitialInterval_in_ms = float(DCForm1.DDSpinEdit158.get()) * 1000

    # Read data file *****************
    ReadWrite_pars.ReadDataFile(fname)
    #*********************************

    # Set T_end
    DCForm1.DDSpinEdit3.set(globs.T_end)

    # Extract the number of the trace from the file extension
    extract_trace_number(fname)

    # Drawing
    DCForm1.DrawArrays()
    DCForm1.notebook.select(DCForm1.tab1)


    # # Extract the type of protocol from the file extension
    # if '.VC' in fname:
    #     DCForm1.RadioGroup2_value.set(5)  # Protocol
    # else:
    #     DCForm1.RadioGroup2_value.set(1)


def export_data_in_clampfit_format():
    fname = NameOfLoadedFile[:NameOfLoadedFile.find('_Err') - 1] + '.atf'

    with open(fname, 'w') as bbb:
        # Header
        bbb.write('ATF\t1.0\n')
        bbb.write('7\t3\n')
        bbb.write('"AcquisitionMode=High-Speed Oscilloscope"\n')
        bbb.write('"Comment="\n')
        bbb.write('"YTop=100,2,10"\n')
        bbb.write('"YBottom=-100,-2,-10"\n')
        bbb.write('"SweepStartTimesMS=0"\n')
        bbb.write('"SignalsExported=I,V"\n')
        bbb.write('"Signals="\t"I"\t"V"\n')
        bbb.write('"Time (ms)"\t"Trace #1 (pA)"\t"Trace #1 (mV)"\n')

        # Writing signals
        i = 0
        while i < N_Arr:
            V     = globs.Arr_V[i]
            Ireal = globs.Arr_Ireal[i]
            Icom  = globs.Arr_Icom[i]
            time  = globs.Arr_t[i]

            bbb.write(f"{time:10.3f}\t{Icom:10.3f}\t{V:10.3f}\t\n")
            i += 1

    bbb.close()
